import os
from bliss.shell.standard import wm
from bliss import setup_globals
from bliss import current_session


def ws():
    """where slit: helper function"""
    return wm(setup_globals.slit_vertical_gap, setup_globals.slit_vertical_offset)


def user_script_homedir_demo():
    current_session.user_script_homedir(
        os.path.join(os.environ["DEMO_ROOT"], "user_scripts")
    )


def user_script_homedir_oda():
    import blissoda

    current_session.user_script_homedir(
        os.path.join(os.path.abspath(blissoda.__path__[0]), "demo", "user_scripts")
    )
