import os
import sys
import socket
from bliss.shell.main import main as _main
import blissdemo


def main() -> int:
    os.environ["TANGO_HOST"] = "%s:%d" % (socket.gethostname(), 10000)
    os.environ["BEACON_HOST"] = "%s:%d" % (socket.gethostname(), 10001)
    os.environ["DEMO_ROOT"] = os.path.realpath(blissdemo.__path__[0])
    return _main(argv=[sys.argv[0], "-s", "demo_session"])


if __name__ == "__main__":
    sys.exit(main())
