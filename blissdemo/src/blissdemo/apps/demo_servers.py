import os
import sys
import redis
import socket
import tempfile
import shutil
import gevent
from docopt import docopt
from ruamel.yaml import YAML
from contextlib import contextmanager, ExitStack
from typing import List, NamedTuple, Optional, Iterator, Tuple, Dict

import blissdemo
from blissdemo import demo_data
from blissdemo import demo_configuration
from bliss.tango.clients import utils as tango_utils
from .processes import ProcessInfo, start_process, start_context, cleanup_process

CLI = """
Usage: demo-servers [--lima-environment=<arg>]
                    [--oda-environment=<arg>]
                    [--beacon-port=<arg>]
                    [--tango-port=<arg>]
                    [--redis-port=<arg>]
                    [--redis-data-port=<arg>]
                    [--timeout=<arg>]

Options:
    --lima-environment=<arg>     Lima simulation Conda environment name
                                 (if unset uses env:LIMA_SIMULATOR_CONDA_ENV)
    --oda-environment=<arg>      Online data processing Conda environment name
                                 (if unset uses env:DEMO_ODA_CONDA_ENV)
    --tango-port=<arg>           Tango database server port [default: 10000]
    --beacon-port=<arg>          Beacon server port [default: 10001]
    --redis-port=<arg>           Redis server for stats [default: 10002]
    --redis-data-port=<arg>      Redis server for data [default: 10003]
    --timeout=<arg>              Timeout for a server to start [default: 60]
"""


class BeaconPorts(NamedTuple):
    beacon_port: int
    tango_port: int
    redis_port: int
    redis_data_port: int


def wait_redis(ports: BeaconPorts, timeout: float = 10) -> None:
    conn = None
    exception = None
    try:
        with gevent.Timeout(timeout):
            while conn is None:
                try:
                    conn = redis.Redis(host=socket.gethostname(), port=ports.redis_port)

                    # disable .rdb files saving (redis persistence)
                    conn.config_set("SAVE", "")
                except Exception as e:
                    exception = e
                    gevent.sleep(1)
    except gevent.Timeout:
        raise RuntimeError("ODA worker did not start") from exception
    return conn


def wait_beacon(ports: BeaconPorts, timeout: float = 10) -> None:
    tango_utils.wait_tango_db(
        host="localhost",
        port=ports.tango_port,
        db=2,
        timeout=timeout,
    )
    wait_redis(ports, timeout=timeout)


def start_beacon(
    stack: ExitStack,
    wait_tasks: List[gevent.Greenlet],
    processes: List[ProcessInfo],
    demo_resources: str,
    cli_options: dict,
) -> None:
    redis_uds = os.path.join(demo_resources, "redis_demo.sock")
    redis_data_uds = os.path.join(demo_resources, "redis_data_demo.sock")
    db_path = os.path.join(demo_resources, "configuration")

    port_names = ["--beacon-port", "--tango-port", "--redis-port", "--redis-data-port"]
    port_list = (int(cli_options[p]) for p in port_names)
    ports = BeaconPorts(*port_list)

    args = [
        sys.executable,
        "-m",
        "bliss.config.conductor.server",
        "--port=%d" % ports.beacon_port,
        "--redis-port=%d" % ports.redis_port,
        "--redis-socket=" + redis_uds,
        "--redis-data-port=%d" % ports.redis_data_port,
        "--redis-data-socket=" + redis_data_uds,
        "--db_path=" + db_path,
        "--tango-port=%d" % ports.tango_port,
        # "--log-level=INFO",
        # "--tango_debug_level=1",
    ]

    ctx = start_process("beacon", args)
    processes.append(stack.enter_context(ctx))

    wait_task = gevent.spawn(wait_beacon, ports, timeout=int(cli_options["--timeout"]))
    wait_tasks.append(wait_task)

    os.environ["TANGO_HOST"] = "%s:%d" % (socket.gethostname(), ports.tango_port)
    os.environ["BEACON_HOST"] = "%s:%d" % (socket.gethostname(), ports.beacon_port)
    os.environ["BEACON_REDIS_PORT"] = "%d" % ports.redis_port

    update_ewoks_config(demo_resources, socket.gethostname(), ports.redis_port)
    update_scan_saving_config(demo_resources)
    set_session_environ(demo_resources)


def update_ewoks_config(demo_resources: str, redis_host: str, redis_port: int) -> None:
    filename = os.path.join(demo_resources, "configuration", "ewoks", "config.yml")
    data = {
        "celery": {
            "broker_url": f"redis://{redis_host}:{redis_port}/3",
            "result_backend": f"redis://{redis_host}:{redis_port}/4",
            "result_serializer": "pickle",
            "accept_content": ["application/json", "application/x-python-serialize"],
            "result_expires": 600,
            "task_remote_tracebacks": True,
        },
    }

    yaml = YAML(pure=True)
    yaml.allow_duplicate_keys = True
    yaml.default_flow_style = False
    with open(filename, "w") as f:
        yaml.dump(data, f)


def update_scan_saving_config(demo_resources: str) -> None:
    filename = os.path.join(demo_resources, "configuration", "__init__.yml")

    yaml = YAML(pure=True)
    yaml.allow_duplicate_keys = True
    yaml.default_flow_style = False

    with open(filename, "r") as f:
        data = yaml.load(f)

    scan_saving = data["scan_saving"]
    for key in ("tmp_data_root", "visitor_data_root", "inhouse_data_root"):
        scan_saving[key] = scan_saving[key].replace("/tmp/scans", demo_resources)

    with open(filename, "w") as f:
        yaml.dump(data, f)


def set_session_environ(demo_resources: str) -> None:
    filename = os.path.join(
        demo_resources, "configuration", "sessions", "scripts", "demo_session.py"
    )
    with open(filename, "r") as f:
        lines = list(f)
    lines = [
        "import os\n",
        f'os.environ["DEMO_TMP_ROOT"] = "{demo_resources}"\n',
        "\n",
    ] + lines
    with open(filename, "w") as f:
        f.writelines(lines)


def conda_cmd(env_name: str, cmd: List[str]) -> List[str]:
    conda = os.environ.get("CONDA_EXE", None)
    if env_name and conda:
        if not env_name:
            return list()
        if os.sep in env_name:
            return [conda, "run", "-p", env_name, "--no-capture-output", *cmd]
        else:
            return [conda, "run", "-n", env_name, "--no-capture-output", *cmd]
    else:
        return cmd


def lima_cmd(server_name: str, cli_options: dict) -> List[str]:
    """
    Create the command line to launch a Lima tango server

    Arguments:
        server_name: Name of the instance of the tango server
    """
    lima_simulator_env = cli_options["--lima-environment"]
    if lima_simulator_env is None:
        lima_simulator_env = os.environ.get("LIMA_SIMULATOR_CONDA_ENV")
    return conda_cmd(lima_simulator_env, ["LimaCCDs", server_name])


def oda_conda_env(cli_options) -> Optional[str]:
    """Returns the name of the conda env defined for the ODA server, else None"""
    oda_env = cli_options["--oda-environment"]
    if oda_env is None:
        return os.environ.get("DEMO_ODA_CONDA_ENV")
    return oda_env


def oda_cmd(cmd: List[str], cli_options: dict) -> Optional[List[str]]:
    """
    Create the command line to launch an ODA server
    """
    return conda_cmd(oda_conda_env(cli_options), cmd)


class TangoDeviceDescription(NamedTuple):
    name: str
    cmd: List[str]
    server_name: str
    cwd: str
    env: Optional[dict] = None


def get_lima_env():
    return {
        "TANGO_HOST": os.environ["TANGO_HOST"],
        "BEACON_REDIS_PORT": os.environ["BEACON_REDIS_PORT"],
        "DEMO_ROOT": os.path.realpath(blissdemo.__path__[0]),
    }


def get_session_env():
    return {
        "BEACON_HOST": os.environ["BEACON_HOST"],
        "TANGO_HOST": os.environ["TANGO_HOST"],
        "DEMO_ROOT": os.path.realpath(blissdemo.__path__[0]),
    }


def get_oda_env():
    return {
        "BEACON_HOST": os.environ["BEACON_HOST"],
        "CELERY_LOADER": os.environ["CELERY_LOADER"],
    }


def tango_servers(cli_options) -> List[TangoDeviceDescription]:
    cwd = os.path.realpath(demo_data.__path__[0])
    lima_env = get_lima_env()
    return [
        TangoDeviceDescription(
            name="id00/limaccds/simulator1",
            cmd=lima_cmd("simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/slits_simulator",
            cmd=lima_cmd("slits_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/tomo_simulator",
            cmd=lima_cmd("tomo_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/diff_simulator",
            cmd=lima_cmd("diff_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/diff2_simulator",
            cmd=lima_cmd("diff2_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/lab6_simulator",
            cmd=lima_cmd("lab6_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/bliss_nxwriter/demo_session",
            cmd=("NexusWriterService", "demo"),
            server_name="NexusWriter",
            cwd=cwd,
        ),
    ]


def start_tango_servers(
    stack: ExitStack,
    wait_tasks: List[gevent.Greenlet],
    processes: List[ProcessInfo],
    cli_options: dict,
) -> None:
    for description in tango_servers(cli_options):
        if not description.cmd:
            continue
        fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
        # device_fqdn = f"{fqdn_prefix}/{device_name}"
        personal_name = description.cmd[-1]
        admin_device_fqdn = (
            f"{fqdn_prefix}/dserver/{description.server_name}/{personal_name}"
        )
        ctx = start_process(
            admin_device_fqdn, description.cmd, cwd=description.cwd, env=description.env
        )
        processes.append(stack.enter_context(ctx))
        wait_task = gevent.spawn(
            tango_utils.wait_tango_device,
            admin_device_fqdn,
            timeout=int(cli_options["--timeout"]),
        )
        wait_tasks.append(wait_task)


def wait_oda_worker(timeout: float = 10) -> None:
    import celery

    app = celery.Celery("ewoks")
    inspect = app.control.inspect()
    try:
        with gevent.Timeout(timeout):
            while not inspect.active():
                gevent.sleep(1)
    except gevent.Timeout:
        raise RuntimeError("ODA worker did not start")


def start_oda_processes(
    stack: ExitStack,
    wait_tasks: List[gevent.Greenlet],
    processes: List[ProcessInfo],
    cli_options: dict,
) -> None:
    os.environ["CELERY_LOADER"] = "ewoksjob.config.EwoksLoader"

    oda_env = get_oda_env()

    # Worker to execute EWOKS workflows
    cmd = ["celery", "-A", "ewoksjob.apps.ewoks", "worker"]
    cmd = oda_cmd(cmd, cli_options)
    if cmd:
        ctx = start_process("oda-worker", cmd, env=oda_env)
        processes.append(stack.enter_context(ctx))
        wait_task = gevent.spawn(wait_oda_worker, timeout=int(cli_options["--timeout"]))
        wait_tasks.append(wait_task)

    # Monitor workflows on http://localhost:5555
    cmd = ["celery", "flower"]
    cmd = oda_cmd(cmd, cli_options)
    if cmd:
        ctx = start_process("oda-monitor", cmd, env=oda_env)
        processes.append(stack.enter_context(ctx))

    # Trigger workflows based on scan_info
    cmd = ["blissoda", "workflows", "demo_session", "--log=info"]
    if oda_cmd(cmd, cli_options):
        ctx = start_process("oda-submitter", cmd)
        processes.append(stack.enter_context(ctx))


def bordered_text(text: str) -> str:
    lines = text.splitlines()
    lines = [line.strip() for line in lines]
    width = max([len(line) for line in lines])
    for i, line in enumerate(lines):
        before = (width - len(line)) // 2
        after = (width - len(line) + 1) // 2
        line = "# " + " " * before + line + " " * after + " #"
        lines[i] = line
    lines.insert(0, "#" * (width + 4))
    lines.append("#" * (width + 4))
    return "\n".join(lines)


def cmd_string(cmd: str, env: Dict[str, str]) -> str:
    lst = [f"{k}={v}" for k, v in env.items()]
    lst.append(cmd)
    return " ".join(lst)


def print_start_message() -> None:
    env = get_session_env()
    env = " ".join([f'{k}="{v}"' for k, v in env.items()])

    text = f"""Start BLISS in another Terminal using

    > bliss-demo-session

    or

    > {env} bliss -s demo_session

    Press CTRL+C to quit this process
    """
    print(bordered_text(text))


def run(demo_resources: str, cli_options: dict):
    processes: List[ProcessInfo] = list()
    try:
        print(bordered_text("    Starting the demo servers    "))
        with start_context() as (stack, wait_tasks):
            start_beacon(stack, wait_tasks, processes, demo_resources, cli_options)
        with start_context() as (stack, wait_tasks):
            start_tango_servers(stack, wait_tasks, processes, cli_options)
            if oda_conda_env(cli_options):
                start_oda_processes(stack, wait_tasks, processes, cli_options)
        print_start_message()
        processes[0].process.wait()
    except KeyboardInterrupt:
        pass
    finally:
        print(bordered_text("    Stopping the demo servers    "))
        for pinfo in processes:
            cleanup_process(pinfo)


@contextmanager
def setup_resource_files() -> Iterator[Tuple[str, str]]:
    """Setup the configuration files"""
    demo_resources = tempfile.mkdtemp(prefix="demo_resources")
    print("Demo resource directory:", demo_resources)
    dest_configuration = os.path.join(demo_resources, "configuration")
    src_configuration = os.path.abspath(demo_configuration.__path__[0])
    shutil.copytree(
        src_configuration,
        dest_configuration,
        ignore=shutil.ignore_patterns("*.pyc", "__init__.py", "__pycache__"),
    )
    try:
        yield demo_resources
    finally:
        shutil.rmtree(demo_resources)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    cli_options = docopt(CLI, argv=argv[1:])
    with setup_resource_files() as demo_resources:
        run(demo_resources, cli_options)
        print(bordered_text("    Done    "))


if __name__ == "__main__":
    sys.exit(main())
