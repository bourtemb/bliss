.ONESHELL: # Do not execute each line in a separate shell
SHELL := /bin/bash
CONDA_INIT := . $$(conda info --base)/etc/profile.d/conda.sh
CYAN := \033[36m
YELLOW := \033[33m
GREEN := \033[32m
END := \033[m

# Make arguments with defaults
PYTHON_VERSION ?= 3.9
NAME ?= bliss_env
LIMA_NAME ?= lima_env
ODA_NAME ?= oda_env

all: help

help:
	@ # '@' begins each target to silence makefile
	printf "${CYAN}#############################################################################${END}\n"
	printf "MAKEFILE TO CREATE AND CONFIGURE CONDA ENVIRONMENTS FOR YOU.\n"
	printf "\n"
	printf "IMPORTANT: All commands are LAZY, no package is updated unless it is strictly\n"
	printf "required. It is up to you to update a particular package, or to start a fresh\n"
	printf "environment to get most recent version of everything.\n"
	printf "\n"
	printf "COMMANDS:\n"
	printf "    ${YELLOW}make bl_env [NAME=<name>] [PYTHON_VERSION=3.9]${END}\n"
	printf "        (typical ESRF beamline installation)\n"
	printf "        - Creates a conda environment for BLISS with the name you provide (default: ${NAME}).\n"
	printf "        - Bliss and blissdata are installed from sources (pip -e).\n"
	printf "    ${YELLOW}make dev_env [NAME=<name>] [LIMA_NAME=<name>] [PYTHON_VERSION=3.9]${END}\n"
	printf "        (use this command to set up an ideal environment to develop with BLISS)\n"
	printf "        - Creates a conda environment for BLISS with the name you provide (default: ${NAME}).\n"
	printf "        - Bliss and blissdata are installed from sources (pip -e)\n"
	printf "          with dev and test tools like black and pytest.\n"
	printf "        - Creates a conda environment with the name you provide (default: ${LIMA_NAME})\n"
	printf "        - Lima simulator plugins are installed from source (pip -e).\n"
	printf "    ${YELLOW}make demo_env [NAME=<name>] [LIMA_NAME=<name>] [PYTHON_VERSION=3.9]${END}\n"
	printf "        (use this command to set up an ideal environment to demonstration of BLISS)\n"
	printf "        - Creates a conda environment for BLISS with the name you provide (default: ${NAME}).\n"
	printf "        - Bliss, blissdata and blissdemo are installed from sources (pip -e).\n"
	printf "        - Create a conda environment with the name you provide (default: ${LIMA_NAME})\n"
	printf "        - Lima simulator plugins are installed from source (pip -e).\n"
	printf "    ${YELLOW}make demo_with_oda_env [NAME=<name>] [LIMA_NAME=<name>] [ODA_NAME=<name>] [PYTHON_VERSION=3.9]${END}\n"
	printf "        (use this command to set up an ideal environment to demonstration of BLISS)\n"
	printf "        - Creates a conda environment for BLISS with the name you provide (default: ${NAME}).\n"
	printf "        - Bliss, blissdata and blissdemo are installed from sources (pip -e).\n"
	printf "        - Create a conda environment with the name you provide (default: ${LIMA_NAME})\n"
	printf "        - Lima simulator plugins are installed from source (pip -e).\n"
	printf "        - Create a conda environment with the name you provide (default: ${ODA_NAME}).\n"
	printf "        - Blissdemo is installed from source (pip -e) with worker dependencies.\n"
	printf "${CYAN}#############################################################################${END}\n"


_check_mamba:
	@which mamba > /dev/null || ( printf \
	"${YELLOW}This makefile use Mamba, a drop-in replacement for Conda, but faster.\n\
	Please install it with:\n\
	    conda install mamba -n base -c conda-forge\n${END}" && exit 1 )


_ensure_bliss_env: _check_mamba
	@$(CONDA_INIT)
	printf "${CYAN}Ensure \"${NAME}\" env exists${END}\n"
	conda activate $(NAME) 2> /dev/null || ( \
	 	printf "${YELLOW}Creating empty ${NAME} environment...${END}\n"; \
		mamba create --name ${NAME} --quiet --no-banner )
	printf "${YELLOW}Done.${END}\n"


_ensure_lima_env: _check_mamba
	@$(CONDA_INIT)
	printf "${CYAN}Ensure \"${LIMA_NAME}\" env exists${END}\n"
	conda activate $(LIMA_NAME) 2> /dev/null || ( \
	 	printf "${YELLOW}Creating ${LIMA_NAME} environment...${END}\n"; \
		mamba env create --name ${LIMA_NAME} -f ./bliss_lima_simulators/conda-environment.yml --quiet )
	###############################################
	# INSTALL ADDITIONAL, SPECIFIC DEMO CODE
	conda run -n ${LIMA_NAME} pip install -e ./bliss_lima_simulators
	###############################################
	printf "${YELLOW}Done.${END}\n"


_ensure_oda_env: _check_mamba
	@$(CONDA_INIT)
	printf "${CYAN}Ensure \"${ODA_NAME}\" env exists${END}\n"
	conda activate $(ODA_NAME) 2> /dev/null || ( \
	 	printf "${YELLOW}Creating ${ODA_NAME} environment...${END}\n"; \
		mamba env create --name ${ODA_NAME} -f ./blissdemo/conda-environment.yml --quiet )
	###############################################
	# INSTALL ADDITIONAL, SPECIFIC DEMO CODE
	conda run -n ${ODA_NAME} pip install -e ./blissdemo[worker]
	###############################################
	printf "${YELLOW}Done.${END}\n"


bl_env: _ensure_bliss_env
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME}...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss and blissdata from sources...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		-e . \
		-e ./blissdata || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:\n    conda activate ${NAME}${END}\n"


dev_env: _ensure_bliss_env _ensure_lima_env
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME} env...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	echo "export LIMA_SIMULATOR_CONDA_ENV=${LIMA_NAME}" >> $$CONDA_PREFIX/etc/conda/activate.d/blissenv.sh
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		--file conda-requirements-dev.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss[dev] and blissdata[dev] from sources...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		-e .[dev] \
		-e ./blissdata[dev] || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:\n    conda activate ${NAME}${END}\n"


demo_env: _ensure_bliss_env _ensure_lima_env
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME} env...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	echo "export LIMA_SIMULATOR_CONDA_ENV=${LIMA_NAME}" >> $$CONDA_PREFIX/etc/conda/activate.d/blissenv.sh
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		--file conda-requirements-dev.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss, blissdata and blissdemo...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		-e . \
		-e ./blissdata \
		-e ./blissdemo || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:${END}\n"
	printf "${GREEN}    conda activate ${NAME}${END}\n"
	printf "${GREEN}Start the demo servers in another terminal:${END}\n"
	printf "${GREEN}    bliss-demo-servers${END}\n"


demo_with_oda_env: _ensure_bliss_env _ensure_lima_env _ensure_oda_env
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME} env...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	echo "export LIMA_SIMULATOR_CONDA_ENV=${LIMA_NAME}" >> $$CONDA_PREFIX/etc/conda/activate.d/blissenv.sh
	echo "export DEMO_ODA_CONDA_ENV=${ODA_NAME}" >> $$CONDA_PREFIX/etc/conda/activate.d/blissenv.sh
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		--file conda-requirements-dev.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss, blissdata and blissdemo...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		-e . \
		-e ./blissdata \
		-e ./blissdemo[client] || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:${END}\n"
	printf "${GREEN}    conda activate ${NAME}${END}\n"
	printf "${GREEN}Start the demo servers in another terminal:${END}\n"
	printf "${GREEN}    bliss-demo-servers${END}\n"

.PHONY: _check_mamba _ensure_bliss_env _ensure_lima_env _ensure_oda_env bl_env dev_env demo_env demo_with_oda_env
