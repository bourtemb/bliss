#!/usr/bin/env python
import os
import sys
import json
import pathlib
import subprocess
from setuptools.config import setupcfg

bliss_root_path = pathlib.Path(__file__).parent.parent
setup_cfg = setupcfg.read_configuration(bliss_root_path / "setup.cfg")

if __name__ == "__main__":
    version = sys.argv[1]
    setup_version = setup_cfg["metadata"]["version"]

    # Ensure bliss version is the same in setup.cfg and commit message
    if version != setup_version:
        print("Release version inconsistency:")
        print(f"    requested version: {version}")
        print(f"    setup.cfg version: {setup_version}")
        print("Abort.")
        sys.exit(1)

    # Uploading conda packages to anaconda.org
    assets = []
    for pkg in pathlib.Path("conda_packages").glob("**/*.tar.bz2"):
        print(f"Uploading {pkg.name} to https://anaconda.org...")
        proc = subprocess.run(
            [
                "anaconda",
                f"--token={os.environ['ANACONDA_TOKEN']}",
                "upload",
                "--user=esrf-bcu",
                pkg.absolute(),
            ],
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        if proc.returncode != 0:
            print(proc.stdout)
            sys.exit(1)

        for line in proc.stdout.split("\n"):
            if line.startswith("Uploading file"):
                pkg_path = line.split('"')[1].split("/")
                pkg_path.insert(0, "https://anaconda.org")
                pkg_path.insert(4, "download")
                pkg_url = "/".join(pkg_path)
                break
        else:
            raise Exception("anaconda-client output parsing failed")
        assets.append(dict(name=pkg.name, url=pkg_url, link_type="package"))
        print(f"Success, package available at {pkg_url}")

    # Uploading python packages to pypi.org
    for pkg in pathlib.Path("dist").glob("**/*"):
        print(f"START UPLOADING {pkg.name}")
        proc = subprocess.run(
            [
                "twine",
                "upload",
                "--repository",
                "pypi",
                pkg.absolute(),
            ],
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        if proc.returncode != 0:
            print(proc.stdout)
            sys.exit(1)

        lines = proc.stdout.split("\n")
        for i, line in enumerate(lines):
            if line == "View at:":
                pkg_url = lines[i + 1]
                break
        else:
            raise Exception("twine output parsing failed")
        assets.append(dict(name=pkg.name, url=lines[i + 1], link_type="package"))
        print(f"Success, package available at {pkg_url}")

    # Publishing release on https://gitlab.esrf.fr/bliss with links to the freshly uploaded packages
    name = f"Bliss {version}"
    description = """# Bliss package repositories
[<img width="25" height="auto" src="https://anaconda.org/static/img/anaconda-symbol.svg"> **Bliss on Anaconda**](https://anaconda.org/esrf-bcu/bliss/)

[<img width="25" height="auto" src="https://pypi.org/static/images/logo-small.95de8436.svg"> **Bliss on PyPI**](https://pypi.org/project/bliss/)

# Changelog
See the [CHANGELOG](https://gitlab.esrf.fr/bliss/bliss/-/blob/master/CHANGELOG.md)."""

    "See the [CHANGELOG](https://gitlab.esrf.fr/bliss/bliss/-/blob/master/CHANGELOG.md)."
    print(f"Creating Gitlab {name} release")
    proc = subprocess.run(
        [
            "release-cli",
            "create",
            f"--name={name}",
            f"--tag-name={version}",
            f"--description={description}",
            f"--assets-link={json.dumps(assets)}",
        ],
        text=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    if proc.returncode != 0:
        print(proc.stdout)
        sys.exit(1)
    print(f"Success, {name} have been released !")
