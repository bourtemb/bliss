# Non-python dependencies
# Note: Some packages are available with pip, but they rely on external dependencies.
#       For example, pytango requires tango that pip cannot install.
#       We use the conda version then, because conda will install tango for us.
graphviz
hkl>=5.0.0
pyopengl==3.1.5 # see https://github.com/mcfletch/pyopengl/issues/88
pyqt>=5.12
pytango>=9.3.1,<9.4,!=9.3.5 # see https://gitlab.com/tango-controls/pytango/-/issues/463#note_1095279955
redis-server>=6.2.6,<7
tailon>=1.1.0
tmux>=3.2a
cxx-compiler  # to build wheels with c-extensions (pymca)