
# MUSST controller

Designed by ISG group, the MUSST board is an NIM module that produces trigger patterns synchronized with
external events.

* **M** ultipurpose
* **U** nit for
* **S** ynchronization
* **S** equencing and
* **T** rigering

It can also be used to:

* read encoder signals
* read ADC values
* read MCA spectrum

The MUSST is controlled via a serial or a GPIB connection. GPIB connection is
faster and must be used to use the MUSST in continuous scans.


## References

* BCU Wiki page: http://wikiserv.esrf.fr/bliss/index.php/Musst
* ISG wiki pages: http://wikiserv.esrf.fr/esl/index.php/Category:MUSST


## Installation

!!! note
    For ESRF users:

    * If using a PCI board:
        - Install GPIB drivers (debian8: `blissinstaller`+`bliss_driver config`; Ubuntu: ansible)
        - Install GPIB Tango device server: `GpibController_PCI` (debian8: `blissinstaller`; Ubuntu: ansible)
        - Declare tango device. ressources example:
        ```
        GpibController_PCI/lmellab/DEVICE/GpibController: "pel/gpib/lmellab"
        pel/gpib/lmellab->GpibBoardName: gpib0
        pel/gpib/lmellab->GpibDeviceTimeOut: 10
        ```
        - Configure server startup script

## Configuration

Example of simple YAML configuration file:

```yaml
name: musst_cc1
class: musst
plugin: generic
gpib:
  url: tango_gpib_device_server://id42/gpib_1/0   # PCI board
  # url: enet://gpibid156.esrf.fr                 # Enet gpib
  pad: 13                                         # primary address
  timeout: 3.                                     # in seconds
channels:
  - type: cnt
    channel: timer
    label: "lab_musst_timer"
    counter_name: musst_timer  # declare a counter associated to that channel
  - type: encoder
    channel: 1
    label: "lab_enc_samy"
    counter_name: enc_samy     # declare a counter
    counter_mode: SINGLE       # declare a mode for that counter

```


It is then possible to use a MUSST counter in a scan or a count:
```python
DEMO [5]: ct(1, musst_sxm.counters.enc_samy)
Mon Feb 10 21:51:44 2020

enc_samy =     393803.0 (    393803.0/s)
  Out [5]: Scan(number=1, name=ct, path=)
```



### Config parameters

* **name**: the controller's name
* **config_tree**: controller configuration. In this dictionary we need to have:

* **gpib**:
    - **url**: url of the gpib controller  ex: `enet://gpib42.esrf.fr`
    - **pad**: primary address of the musst controller
    - **timeout**: communication timeout, default is 1s
    - **eos**: end of line termination

* **musst_prg_root**: default path for musst programs
* **block_size**: default is 8k but can be lowered to 512 depend on gpib.
* **one_line_programing**: default is False we send several lines to program the musst
* **channels**: list of configured channels, in this dictionary we need to have:
    * **type**: channel type (`cnt`, `encoder`, `ssi`, `adc5`, `adc10` or `switch`)
    * **channel**: channel number
    * **label**: the name alias for the channels
    * **name**: use to reference an external switch
    * **counter_name**: if present, use that name to create a counter associated to that channel
    * **counter_mode**: [Counter Sampling Mode](dev_counters.md#sampling-counter-modes)

!!! note
  **type** `cnt` is associated with integrating counters.
  **type** [`encoder`, `ssi`, `adc5`, `adc10`] is associated with sampling counters.
  **counter_mode** is only available for sampling counter types.

## Commands

### status

```python


DEMO [1]: musst_sxm
 Out [1]: MUSST card: musst_sxm, MUSST 01.01a
          GPIB type=TANGO_DEVICE_SERVER url='tango_gpib_device_server://id42/gpib_lid423/0'
               primary address='13' secondary address='0' tmo='13' timeout(s)='10.0' eol=''
          TIMEBASE: 1MHZ
          MEMORY:
                   MCA:     size (32b values):     2048, buffers:      128
                   STORAGE: size (32b values):   262144, buffers:        1
          CHANNELS:
                   CH1 ( RUN):         -2 -  ENC DIR
                   CH2 ( RUN):    4813076 -  ENC DIR
                   CH3 ( RUN):     566996 -  ENC DIR
                   CH4 ( RUN):     109294 -  ENC DIR
                   CH5 ( RUN):     -12255 -  ENC DIR
                   CH6 (STOP):          0 -  CNT
```

To set a channel to desired value:
```python
musst_sxm.set_variable("CH3", 123)
```

Direct communication:
```python
DEMO [21]: musst_sxm.putget("?CH CH3")
 Out [21]: '53427 RUN'
```


## Switch

## MUSST MCA

## MUSST MCA

## MUSST Programming
