# Microdiffractometer (MD2-S, MD3, MD3-UP) Motors
Micro Difractometers designed by EMBL and commercialised by ARINAX are
used to perform crystallography experiments.
In bliss their control is separated in two parts: motors and equipment.
The configuration below only concerns motors.


![MD2-S](img/md2s.png) ![MD3](img/md3.png)

Some information about the device:
https://www.arinax.com/md2-s-x-ray-microdiffractometer


### Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
NO	| NO      | NO

### Underlining Control

There is a java application, running on windows OS, which provides an
API with Commands and Channels. Bliss is interfaced to this API via the
Exporter Protocol - TCP/IP sockets communication, using ASCII request and
replies.

### Example YAML configuration file ###

```yaml
controller:
  class: MD2
  module: mx.md2
  exporter_address: "widxxmicrodiff:9001"
  axes:
      -
        name: sampx
        root_name: "CentringX"
        steps_per_unit: 1
      -
        name: sampy
        root_name: "CentringY"
        steps_per_unit: 1
      -
        name: phix
        root_name: "AlignmentX"
        steps_per_unit: 1
      -
        name: phiy
        root_name: "AlignmentY"
        steps_per_unit: 1
      -
        name: phiz
        root_name: "AlignmentZ"
        steps_per_unit: 1
```

### Configuration
The configuration yml file should provide those parameters:

* `class`: always MD2
* `exporter_address`: should contain hostname:port - the name of the computer
where the java application runs and the socket port configured in the
application.
* `axes` should map *name* of each motor object in bliss with *root_name* defined by the API.

Usually the only parameter that needs to be changed is the `exporter_address`.
