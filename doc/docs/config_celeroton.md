#Celeroton Fast Chopper

The Celeroton Fast Chopper is using serial line binary protocol.
A command always consist of a request package and a reply package,
containing at least an acknowledgment or an error code. The format is:
```
Request:
         |-------------------- length ---------------------|
------------------------------------------------------------
| length | command | data[0] | ... | data [n-1] | checksum |
------------------------------------------------------------
|------------------ checksum -------------------|
```

The command byte of the reply package is used to acknowledge the previously
requested command. If the acknowledgment is positive, the command byte is the
repetition of the previously requested command and the following data is then
command specific.
A negative acknowledgment is indicated by the bitwise inverted command byte
followed by two bytes representing a 16 bit error code, transmitted LSB first.

```
Reply for negative acknolegment:
-------------------------------------------------------
| length | ~command | LSB Error | MSB error| checksum |
-------------------------------------------------------
```
Each block is 1 byte. The **checksum** is calculated on all the blocks,
using the formula:
```
checksum = (~(length+command+sum(data[0]...data[n-1]))+1) & 0xff
```
The parameters of the serial (RS232) line are:

  - 8 bits
  - no parity
  - 1 stop bit
  - 57600 baud

Manifacturer: © Celeroton AG. www.celeroton.com

## YAML configuration example
```yaml
name: fast_chopper
class: Celeroton
serial:
   url: "rfc2217://lid293:28010"
or
   url: id29/chopper/fast
```

!!! warning "serial"
    url can be either ser2net or Tango

The plugin for this controller is `generic`.
```yaml
   plugin: generic
```
should either be in \_\_init__.yml in the same directory or added to the
above configuration.
