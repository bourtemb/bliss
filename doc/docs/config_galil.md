# Galil motor controller

## Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
NO      | NO      |      NO

## Configuration

* `url`: controller hostname or IP address
* `model_id`: can be: `DMC50000` `DMC52000` `DMC4000` `DMC4010` `DMC4103` `DMC4200`
`DMC30010` `DMC2103` `DMC2105` `DMC1802` `DMC1806` `RIO47000` `EDD37010`

!!!note
    Tested models are for now: `DMC4010` and `DMC30010`

Axis parameters:

* `name`: 
* `channel`: 
* `steps_per_unit`: 
* `velocity`: 
* `acceleration`: 
* `unit`: 


### YAML configuration file example

```yaml
-  class: GalilDMC
   plugin: emotion
   module: motors.galil.galildmc
   tcp:
     url: galid42b
     eol: "\r"
   model_id: DMC30010
   axes:
    - name: leg1
      channel: A
      steps_per_unit: 26222.2        #
      velocity: 1.4                  #
      acceleration: 2.8              #
      unit: mm                       #
```










## Specific Configuration

* export mode only ?
* read/write from/to a config file ?

### Controller Configuration ???

* `limit_switch`: `LOW_LEVEL` | `HIGH_LEVEL`
* `home_switch`: `LOW_LEVEL` | `HIGH_LEVEL`
* `latch_polarity`: `LOW_LEVEL` | `HIGH_LEVEL`
* `sampling_period`: Sampling period of the control loop in ms
* `vect_smoothing`: only for DMC_2000 models ?
* `vect_acceleration`: acceleration rate of the vector in a coordinated motion sequence. default value is 262144
* `vect_deceleration`: deceleration rate of the vector in a coordinated motion sequence. default value is 262144
* `vect_slewrate`: speed of the vector in a coordinated motion sequence. Default value is 8192
* `axes_count`:




### Axis Configuration ???



* `motor_type`: is the motor type, default value is `SERVO`. Values can be:
    - `SERVO`: (1) : Servo Motor
    - `INV_SERVO`: (-1) : Servo motor with reversed polarity
    - `STEPPER_LOW`: (2) : Step motor with active low step pulses
    - `INV_STEPPER_LOW`: (2.5) : Step motor with reversed direction and active low step pulses
    - `STEPPER_HIGH`: (-2) : Step motor with active high step pulses
    - `INV_STEPPER_HIGH`: (-2.5) : Step motor with reversed direction and active high step pulses
* axis_ratio`: 
* `encoder_type`: the quadrature type or the pulse and direction type. Default value is QUADRA. Values can be:
    - `QUADRA`: (0): Normal quadrature
    - `PULSE`: (1): Normal pulse and direction
    - `REVERSED_QUADRA`: (2): Reversed quadrature
    - `REVERSED_PULSE`: (3): Reversed pulse and direction
* `kp`: Proportional Constant. default value is 1.0
* `ki`: Integrator. default value is 6.0
* `kd`: Derivative Constant. default value is 7.0
* `integrator_limit`: limits the effect of the integrator function in the filter to a certain voltage.
For example, integrator_limit 2 limits the output of the integrator of the A-axis to the +/-2 Volt range.
default value is set to the maximum value (9.998)
* `smoothing`: Independent Time Constant. default value is 1.0
* `acceleration`: 
* `deceleration`: 
* `slew_rate`: 
* `off_on_error`: 
* `error_limit`: the magnitude of the position errors for each axis that will trigger an
error condition. default value is 16384
* `cmd_offset`: bias voltage in the motor command output. default value is 0.0
* `torque_limit`: the limit on the motor command output. For example, torque_limit of 5 limits
the motor command output to 5 volts. Maximum output of the motor command is 9.998
volts. default value is set to the maximum.

