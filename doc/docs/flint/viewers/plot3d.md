# 3D plot

This is a plot for general 3D use case.

For now it supports scatters and meshes.

## Example

```python
# create the plot
f = flint()
p = f.get_plot("plot3d", name="My plot")

# describe the items to display
p.add_scatter_item("x", "y", "z", "value", legend="item1",
                   symbol="o", symbol_size=50, lut="viridis")
p.add_mesh_item("vertexes", "faces", legend="item2")

# update the data
p.set_data(
    x=[0, 1, 2], y=[0, 1, 0], z=[0, 0, 1], value=[0, 1, 2],
    vertexes=[[0, 0, 0], [1, 1, 0], [2, 0, 1]],
    faces=[[0, 1, 2]],
)
```
