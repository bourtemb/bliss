# Counters

A **counter** represents an experimental parameter which can be measured during
a scan or a count. Counters are passed to the Scan object in order to select the
experimental parameters that will be measured or, in other words, the data that
will be produced.

The data reading is not performed by the counter itself but by the
**CounterController** that owns the counter. This allows to perform grouped
data reading at the controller level, when multiple counters are owned by the
same device.

The **Counter** object is a placeholder to store information about the counter
and its associated data.

For general definitions and usage, see the
[Counters](bliss_counters.md#counters) section.

![Screenshot](img/CounterControllerStructure.png)


![Screenshot](img/counter_classes.svg)



## Standard counters

### Counter

The base class for all counters (mandatory).

It defines the common properties and methods shared by all counters:

- **name**: the counter short name (mandatory)
- **_counter_controller**: the *CounterController* which own this counter (mandatory)
- **dtype**: the data type associated to this counter (numpy dtype) (default is `float`)
- **shape**: the shape of the data associated to this counter (`()` for 0D)
- **unit**: the data unit (default is `None`)
- **fullname**: the concatenation of its name and controller name in the form `controller_name:counter_name`
- **conversion_function**: a function to convert data (default is `None`)
- **scan_metadata()**: return the metadata dictionary that will be stored in the scan data file (default is `None`)
- **__ info __()**: return a text description of the counter (displayed while typing the name of the counter in the shell).

### SamplingCounter (SC)

Designed for instantaneous data reading (like reading the actual value of a
sensor/channel).  Depending on its **mode** property, the reading is performed
once or repeated as many time as possible during the given **counting time** to
provide averaged or statistical measurements.  Also, sampling counters provide
the **raw_read** property to read data at any time (even outside the context of
a scan).


### IntegratingCounter (IC)

Designed for data that are integrated over the **counting time** and buffered by
the counting device. A polling mechanism ( *Reading* loop) empty the device
buffer and retrieves the data chunks. This kind of counter can be bound to a
master device that propagates its counting time to the integrating counters
that depend on it.


## CounterControllers

A **Counter** is always managed by a **CounterController** (see
`mycounter._counter_controller`).

A *CounterController* is a container for counters of the same kind, that usually
share the same hardware.

* **CounterController (CC)** is the base class for all counter controllers (mandatory):

    It defines the common properties and methods shared by all counter controllers:

    - **name**: the counter controller name (mandatory)
    - **_master_controller**: an optional master counter controller on top of this one (optional).
    - **fullname**: the concatenation of its name and master controller fullname in the form `master_fullname:controller_name`
    - **counters**: the list of managed counters as a `counter_namespace(self._counters)`
    - **create_counter(counter_class, args, kwargs)**: dedicated method to create a counter and register it into this CC (`self._counters[name]`).
    - **create_chain_node()**: create the default chain node associated to this CC (over-writable).
    - **get_acquisition_object(acq_params, ctrl_params, parent_acq_params)**: return the *AcquisitionObject* associated to this controller (must be implemented in the child class).
    - **get_default_chain_parameters(scan_params, acq_params)**: return the default *acquisition parameters* to be use in the context of a standard step-by-step scan (must be implemented in the child class).
    - **get_current_parameters()**: return an exhaustive dict of the current *controller parameters* (default: return `None`).
    - **apply_parameters()**: load *controller parameters* into hardware controller (called at the beginning of each scan) (default: `pass`)

* **SamplingCounterController (SCC)**: dedicated base class to manage *SamplingCounters*:
    - **get_acquisition_object**: returns `SamplingCounterAcquisitionSlave` (over-writable)
    - **get_default_chain_parameters**:  returns `count_time` and `npoints` (over-writable)

* **IntegratingCounterController (ICC)**: dedicated base class to manage *IntegratingCounters*:
    - **get_acquisition_object**: returns `IntegratingCounterAcquisitionSlave` (over-writable)
    - **get_default_chain_parameters**: returns `count_time` and `npoints` (over-writable)


### Group read
Both **ICC** and **SCC** provide mechanism to perform **group reads** in order
to read many counters at once, if they belong to a common controller able to
read all channels at once.

For example, all ROI counters of one camera are managed by one counter controller
which can retrieve data of all rois at once via one command.

- **SamplingCounterController**: user must implement at least the `read`
method. If the controller is able to read multiple counters at once, overwrite
the `read_all` method (the one called during acquisition).  `*counters` is a
list of one or more counters managed by this CC.

```python

    def read_all(self, *counters):
        """ return the values of the given counters as a list.
            If possible this method should optimize
            the reading of all counters at once.
        """
        # overwrite if hardware can read all the given 'counters' at once
        values = []
        for cnt in counters:
            values.append(self.read(cnt))
        return values

    def read(self, counter):
        """ return the value of the given counter """
        # access hardware and read data corresponding to the given 'counter'
        raise NotImplementedError

```

- **IntegratingCounterController**: user must implement the `get_values` method.
This method must retrieve the latest measurements available from the hardware
since a given `from_index`.  As the hardware may have buffered multiple
measurements, this method must handle a list of data for each counter
(*data_list*). In the returned per counter list, all *data_list* must have the
same length (i.e. same number of measurements).

```python
    def get_values(self, from_index, *counters):
        # access hardware and return latest data since 'from_index'
        # for the given 'counters'.
        # !!! returned (per counter) data lists must have the same length !!!
        raise NotImplementedError
```


## Device controller and counters

While writing a new controller for an hardware device, it is recommended to
declare the **CounterController(s)** as local attribute(s).

The counter controller class could be directly inherited but this would mix the
counter controller methods with the device controller API.

The fact that the device controller may need multiple counter controllers must
be kept in mind.

For example, a device controller may require a *SamplingCounterController* and
an *IntegratingCounterController*.

![Screenshot](img/DeviceControllerStructure.png)

### CounterContainer protocol

In order to tell Bliss that a device controller can be used in a scan as a
counting device, it must inherit from the `CounterContainer` protocol.
Inheriting from this protocol implies that the class implements a `.counters`
property which returns the list of managed counters (as a counter_namespace).


```python
class MyDeviceController(CounterContainer):
    def __init__(self, name, config):
        self.name = name
        self._icc_taggs = ['xxx', 'yyy', 'zzz']
        self._scc = MySamplingCounterController(f"{self.name}_scc")
        self._icc = MyIntegratingCounterController(f"{self.name}_icc")

        for cnt_cfg in config.get('counters', []):
            tag = cnt_cfg['tag']
            if tag in self._icc_taggs:
                self._icc.create_counter(MyIntegratingCounter, cnt_cfg)
            else:
                self._scc.create_counter(MySamplingCounter, cnt_cfg)

    @autocomplete_property
    def counters(self):     # conform to the CounterContainer protocol
        cnts = list(self._scc.counters) + list(self._icc.counters)
        return counter_namespace(cnts)
```


## Examples

### Moco controller with sampling counters

```python
# === The Counter class
class MocoCounter(SamplingCounter):
    def __init__(self, name, config, controller):
        self.role = config["role"]
        if self.role not in controller.moco.values.keys():
            raise RuntimeError(
                f"moco: counter {self.name} role {self.role} does not exists"
            )
        SamplingCounter.__init__(self, name, controller, mode=SamplingMode.LAST)

# === The CounterController class
class MocoCounterController(SamplingCounterController):
    def __init__(self, moco):
        self.moco = moco
        super().__init__(self.moco.name, register_counters=False)
        global_map.register(moco, parents_list=["counters"])

    def read_all(self, *counters):
        self.moco.moco_read_counters()
        values = []
        for cnt in counters:
            values.append(self.moco.values[cnt.role])
        return values

# === The top level controller class (exposed at user level)
class Moco(CounterContainer):
    def __init__(self, name, config_tree):
        self.values = {"outbeam": 0.0, "inbeam": 0.0,}
        self.name = name
        # Communication
        self._cnx = get_comm(config_tree, timeout=3)
        global_map.register(self, children_list=[self._cnx])
        # default config
        self._default_config = config_tree.get("default_config", None)
        # motor
        self.motor = None

        # Counters
        self.counters_controller = MocoCounterController(self)
        self.counters_controller.max_sampling_frequency = config_tree.get(
            "max_sampling_frequency"
        )
        counter_node = config_tree.get("counters")
        for config_dict in counter_node:
            counter_name = config_dict.get("counter_name")
            MocoCounter(counter_name, config_dict, self.counters_controller)

    @property
    def counters(self):
        return self.counters_controller.counters

    def moco_read_counters(self):
        ret_val = self.comm("xxx")
```

### CT2 controller (as master) with integrating counters (as slave)

```python

# === The Counter class
class CT2Counter(IntegratingCounter):
    def __init__(self, name, channel, controller, unit=None):
        self.channel = channel
        super().__init__(name, controller, unit=unit)

    def convert(self, data):
        return data

# === The CounterController class
class CT2CounterController(IntegratingCounterController):
    def __init__(self, name, master_controller):
        super().__init__(name=name, master_controller=master_controller)
        self.counter_indexes = {}

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        from bliss.scanning.acquisition.ct2 import CT2CounterAcquisitionSlave

        if "acq_expo_time" in parent_acq_params:
            acq_params.setdefault("count_time",
                                   parent_acq_params["acq_expo_time"])
        if "npoints" in parent_acq_params:
            acq_params.setdefault("npoints", parent_acq_params["npoints"])

        return CT2CounterAcquisitionSlave(self, ctrl_params=ctrl_params,
                                                **acq_params)

    def get_values(self, from_index, *counters):
        data = self._master_controller.get_data(from_index).T
        if not data.size:
            return len(counters) * (numpy.array(()),)
        result = [
            counter.convert(data[self.counter_indexes[counter]]) \
            for counter in counters
        ]
        return result

# === The main controller which is also a CounterController acting
# === as a master above the CT2CounterController (slave) class
class CT2Controller(CounterController):
    def __init__(self, device_config, name="ct2_cc", **kwargs):
        super().__init__(name=name, register_counters=False)

        # declare an ICC with 'self' as the '_master_controller'
        self._slave = CT2CounterController("ct2_counters_controller", self)

        # Add ct2 counters
        for channel in device_config.get("channels", ()):
            ct_name = channel.get("counter name", None)
            if ct_name:
                address = int(channel["address"])
                self._slave.create_counter(CT2Counter, ct_name, address)

    @property
    def counters(self):
        return self._slave.counters

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        from bliss.scanning.acquisition.ct2 import (
            CT2AcquisitionMaster,
            CT2VarTimeAcquisitionMaster,
        )

        if isinstance(acq_params.get("acq_expo_time"), list):
            return CT2VarTimeAcquisitionMaster(
                self, ctrl_params=ctrl_params, **acq_params
            )
        else:
            return CT2AcquisitionMaster(self,
                                        ctrl_params=ctrl_params,
                                        **acq_params)

    def get_default_chain_parameters(self, scan_params, acq_params):
        # Extract scan parameters
        try:
            npoints = acq_params["npoints"]
        except KeyError:
            npoints = scan_params.get("npoints", 1)

        try:
            acq_expo_time = acq_params["acq_expo_time"]
        except KeyError:
            acq_expo_time = scan_params["count_time"]

        acq_point_period = acq_params.get("acq_point_period")
        acq_mode = acq_params.get("acq_mode", AcqMode.IntTrigMulti)
        prepare_once = acq_params.get("prepare_once", True)
        start_once = acq_params.get("start_once", True)
        read_all_triggers = acq_params.get("read_all_triggers", False)

        params = {}
        params["npoints"] = npoints
        params["acq_expo_time"] = acq_expo_time
        params["acq_point_period"] = acq_point_period
        params["acq_mode"] = acq_mode
        params["prepare_once"] = prepare_once
        params["start_once"] = start_once
        params["read_all_triggers"] = read_all_triggers

        return params

```

<!-- ### Examples  are commented because they are outdated.
I have not remove them totally because they can still be good examples but
somebody has to go through, test and apply proper modifications.
On the mean time, the others up-to-date examples I have added should be enough for now.

Perceval Guillou



```python

class MyCounter(SamplingCounter):
    def __init__(self, name, controller, channel):

        # adding a custom attribute that describes which channel
        # of the hardware device should be read for this counter
        self._channel = channel

        # init the base class and customize default mode and units
        super().__init__(name, controller, mode=SamplingMode.LAST, unit='mA')

    def scan_metadata(self):
        meta = {"kind": "collection", "channel": self._channel}
        return {self.name: meta}

```

```python
class RoiCollectionCounter(IntegratingCounter):
    """ A Counter object used for a collection of Rois """

    def __init__(self, name, controller):
        super().__init__(name, controller)

    def scan_metadata(self):
        params = [roi.get_params() for roi in self._counter_controller.get_rois()]
        xs, ys, ws, hs = zip(*params)
        meta = {"kind": "collection", "x": xs, "y": ys, "width": ws, "height": hs}
        return {self.name: meta}

    @property
    def shape(self):
        """The data shape as used by numpy."""
        return (len(self._counter_controller),)

    @property
    def dtype(self):
        """The data type as used by numpy."""
        return numpy.int32
```

```python
class BaseMcaCounter(Counter):
    def __init__(self, mca, base_name, detector=None):
        self.mca = mca
        self.acquisition_device = None
        self.detector_channel = detector
        self.base_name = base_name

        super().__init__(base_name, mca)

    @property
    def name(self):
        if self.detector_channel is None:
            return self.base_name
        return "{}_det{}".format(self.base_name, self.detector_channel)

    # Extra logic
    def register_device(self, device):
        # Current device
        self.acquisition_device = device
        # Consistency checks
        assert self._counter_controller is self.acquisition_device.device
        if self.detector_channel is not None:
            assert self.detector_channel in self._counter_controller.elements

    def extract_point(self, spectrums, stats):
        raise NotImplementedError

    def feed_point(self, spectrums, stats):
        point = self.extract_point(spectrums, stats)
        return point
``` -->



<!--
## Sampling counter

Depending on the number and the way how channels of the controller are
managed, different designs can be used.

### example 1

A simple 1 channel-counter.

YML configuration:

    plugin: bliss
    class: EMH
    name: emeter2
    unit: volt
    tcp:
       url: em

In this example, BLISS controller has:

* to deal with connection (TCP)


### example 2 : EMH

A controller with multiple channels.

EMH is a 4chan electrometer.

YML configuration:

    plugin: bliss
    class: EMH
    name: emeter2
    unit: volt
    tcp:
       url: emeter2.esrf.fr
    counters:
    - counter_name: e1
      channel: 1
    - counter_name: e2
      channel: 2

!!! note
    In this example, keyword `counter_name` is used instead of
    `name` to avoid to load automatically the counters objects in
    BLISS sessions.


This example BLISS controller has:

* to deal with the connection (TCP)
* to manage channels (returned by `counters()` property)
* to allow a grouped reading of all channels.

NB: the controller file name must be in lower case.

Example from `emh.py`:

    class EmhCounter(SamplingCounter):
        def __init__(self, name, controller, channel, unit=None):

            SamplingCounter.__init__(self, name, controller)
            #                                    ref to the controller
            # the reading of many counters depending on the same
            # controller will be performed using controller.read_all() function

            self.channel = channel
            self.unit = unit
    class EMH(object):
        """
        ESRF - EM#meter controller
        """
        def __init__(self, name, config):

            self.name = name
            self.bliss_config = config

            self.comm = get_comm(config, TCP, eol='\r\n', port=5025)
            # port number is fixed for this device.

            self.counters_list = list()
            for counter_conf in config.get("counters", list()):
                unit = counter_conf.get_inherited("unit")
                counter = EmhCounter(counter_conf["counter_name"],
                                     self,
                                     counter_conf["channel"], unit)
                self.counters_list.append(counter)

        @property
        def counters(self):
            return counter_namespace(self.counters_list)

        def read_all(self, *counters):
            curr_list = self.get_currents()
            vlist = list()

            for counter in counters:
                vlist.append(curr_list[counter.channel - 1])

            return vlist -->



<!-- ## Integrating counter

### example 1

A controller exporting N counters.



## NOTES
![Screenshot](img/counters_hierarchy.svg)

## Calculation counter

Calculation counters can be use to do some computation on raw values
of real counters.

### Simple example

In this example the calculation counter will return the mean of two
real counters.
Real counters are **diode** and **diode2**.

```python
from bliss.common.measurement import CalcCounter
from bliss.scanning.acquisition.calc import CalcHook

# Mean caclulaion
class Mean(CalcHook):
    def prepare(self):
        self.data = {}
    def compute(self,sender,data_dict):
    	# Gathering all needed data to calculate the mean
	# Datas of several counters are not emitted at the same time
        nb_point_to_emit = numpy.inf
        for cnt_name in ('diode','diode2'):
            cnt_data = data_dict.get(cnt_name,[])
            data = self.data.get(cnt_name,[])
            if len(cnt_data):
                data = numpy.append(data,cnt_data)
                self.data[cnt_name]=data
            nb_point_to_emit = min(nb_point_to_emit,len(data))
	# Maybe noting to do
        if not nb_point_to_emit:
            return

        # Calculation
        mean_data = (self.data['diode'][:nb_point_to_emit] +
                     self.data['diode2'][:nb_point_to_emit]) / 2.
        # Removing already computed raw datas
        self.data = {key:data[nb_point_to_emit:]
                     for key,data in self.data.items()}
        # Return name musst be the same as the counter name:
	# **mean** in that case
        return {"mean":mean_data}

mean = CalcCounter("mean",Mean(),diode,diode2)
```



#Tutorial

Use-case examples to export new counters in BLISS.

##Sampling counters

###Simple case, a controller with only one counter

In this example the sampling counter and the controller is the same instance.
and we want to read value from a **tcp server** the emitted random values.

```bash
cat /dev/urandom | nc -k -l -p 3333
```


Bliss counter configuration may look like:

```yaml
- plugin: bliss
  package: simple_random
  class: RandomCnt
  name: rand_cnt
  tcp:
     url: localhost:3333
```

And in file **simple_random.py**:

```python
import struct
from bliss.comm.util import get_comm
from bliss.common.measurement import SamplingCounter

class RandomCnt(SamplingCounter):
    def __init__(self,name,config):
        super().__init__(name,self)
        self.comm = get_comm(config)

    def read(self):
        random_value = self.comm.read(4)
        self.comm.close()
        return struct.unpack('I',random_value)[0]
```

In bliss console:

```
BLISS [1]: rand_cnt = config.get('rand_cnt')
BLISS [2]: rand_cnt.read()
  Out [2]: 2300708583
BLISS [3]: ct(1,rand_cnt)

Mon Jul 29 19:36:49 2019
     dt[s] =          0.0 (         0.0/s)
  rand_cnt = 2136576723.7867134 ( 2136576723.7867134/s)
  Out [3]: Scan(number=3, name=ct, path=<no saving>)
```

### Severals counters sharing same controller

In this example we will export individual counters defined in the configuartion.
The counter controller read all information about the current world population with
the `.read_all` method.

In this case configuration look like:

```yaml
- plugin: bliss
  package: worldometers
  class: WorldCounter
  counters:
      - name: current_world_population
      - name: day_births
      - name: day_deaths
```

The file **worldometers.py**:

```python
from bs4 import BeautifulSoup
from urllib.request import urlopen
from bliss.common.measurement import SamplingCounter

class WorldoMeterCtrl:
    COUNTER_NAME_2_ID = {
        'current_world_population': 'cp1',
        'current_world_population_male': 'cp2',
        'current_world_population_female': 'cp3',
        'day_births' : 'cp7',
        'year_births' : 'cp6',
        'day_deaths': 'cp9',
        'year_deaths': 'cp8',
        }
    @property
    def name(self):
        return 'worldometer'

    def read_all(self,*counters):
        url = 'https://countrymeters.info/en/World'
        html = urlopen(url)
        soup = BeautifulSoup(html, 'html.parser')
        return [self._get_field_value(soup, self.COUNTER_NAME_2_ID[cnt.name])
                for cnt in counters]

    @staticmethod
    def _get_field_value(soup,name):
        return int(soup.find(id=name).getText().replace(',',''))

CONTROLLER = WorldoMeterCtrl()

class WorldCounter(SamplingCounter):
    def __init__(self,name,config):
        super().__init__(name,CONTROLLER)
```

!!! note
    `WorldCounter.read` use the `read_all` of the controller

In bliss console:

```
BLISS [1]: current_world_population = config.get('current_world_population')
BLISS [2]: day_births = config.get('day_births')
BLISS [3]: day_births.read()
  Out [3]: 248807
BLISS [4]: day_deaths = config.get('day_deaths')
BLISS [5]: ct(1,current_world_population,day_births,day_deaths)

Tue Jul 30 15:03:20 2019

                     dt[s] =          0.0 (         0.0/s)
  current_world_population = 7723065258.0 ( 7723065258.0/s)
                day_births =     247874.0 (    247874.0/s)
                day_deaths =      98390.0 (     98390.0/s)
  Out [5]: Scan(number=9, name=ct, path=<no saving>)

BLISS [6]: loopscan(10,.1,current_world_population,day_births,day_deaths,save=False)

Scan 10 Tue Jul 30 15:04:00 2019 <no saving> default user = seb
loopscan 10 0.1

           #         dt[s]  current_world_population    day_births    day_deaths
           0             0               7.72307e+09        248062         98464
           1      0.723097               7.72307e+09        248066         98466
           2       1.45873               7.72307e+09        248071         98468
           3       2.21063               7.72307e+09        248075         98470
           4       2.94659               7.72307e+09        248075         98470
           5       3.73903               7.72307e+09        248080         98472
           6       4.49515               7.72307e+09        248085         98473
           7       5.21481               7.72307e+09        248089         98475
           8       5.97999               7.72307e+09        248089         98475
           9       6.78664               7.72307e+09        248094         98477

Took 0:00:07.647674
  Out [6]: Scan(number=10, name=loopscan, path=<no saving>)
```

### A controller with severals counters

Here we have a controller which hold all sensor of a linux pc as a bliss SamplingCounter.
Basically we use the `sensors` linux command.

The configuration:

```yaml
- plugin: bliss
  package: linux_sensors
  class: Sensor
  name: sensor
```

The file **linux_sensors.py**

```python
import re
from gevent import subprocess
from bliss.common.measurement import SamplingCounter,counter_namespace
from bliss.common.utils import autocomplete_property

class Sensor:
    def __init__(self,name,config):
        self.name = name

    @autocomplete_property
    def counters(self):
        counters = [SamplingCounter(name,self) for name in self._read_sensors()]
        return counter_namespace(counters)

    def read_all(self,*counters):
        sensors_values = self._read_sensors()
        return [sensors_values[cnt.name] for cnt in counters]

    def _read_sensors(self):
        p = subprocess.Popen("sensors",stdout=subprocess.PIPE)
        exp = re.compile(b"^(.+?): *[+-]?(\d+(\.\d*)?|\.\d+)")
        name2values = dict()
        name2nb = dict()
        for line in p.stdout.readlines():
            g = exp.match(line)
            if g:
                name,value = g.group(1),g.group(2)
                name = name.decode()
                name = name.replace(' ','_')
                nb = name2nb.setdefault(name,-1) + 1
                name2nb[name] = nb
                if nb :
                    name2values[f'{name}_{nb}'] = value
                else:
                    name2values[name] = value
        return name2values
```

!!! note
    property `.counters` is used by standard scans to get counters of
    a controller.

In bliss console:

```
BLISS [1]: ls = config.get('linux_sensor')
BLISS [2]: ct(1,ls)

Wed Jul 31 10:53:46 2019

          dt[s] =          0.0 (         0.0/s)
            CPU = 42.561797752808985 ( 42.561797752808985/s)
         Core_0 = 38.04494382022472 ( 38.04494382022472/s)
         Core_1 = 40.04494382022472 ( 40.04494382022472/s)
         Core_2 = 38.17977528089887 ( 38.17977528089887/s)
         Core_3 = 38.95505617977528 ( 38.95505617977528/s)
      Other_Fan =          0.0 (         0.0/s)
    Other_Fan_1 = 601.943820224719 ( 601.943820224719/s)
   Package_id_0 = 43.04494382022472 ( 43.04494382022472/s)
  Processor_Fan = 1000.0337078651686 ( 1000.0337078651686/s)
         SODIMM =          0.0 (         0.0/s)
       SODIMM_1 =         36.0 (        36.0/s)
       SODIMM_2 =         32.0 (        32.0/s)
  Out [2]: Scan(number=14, name=ct, path=<no saving>)

BLISS [3]: loopscan(5,0.1,ls,save=False)

Scan 15 Wed Jul 31 10:54:59 2019 <no saving> default user = seb
loopscan 5 0.1

           #         dt[s]           CPU        Core_0        Core_1        Core_2        Core_3     Other_Fan   Other_Fan_1  Package_id_0  Processor_Fan        SODIMM      SODIMM_1      SODIMM_2
           0             0            40            36            34            39            38             0       597.286            41        1004.29             0            36            32
           1      0.102002            41            36            34            39            38             0       588.857            41           1005             0            36            32
           2      0.202884            41            36            34            39            38             0         591.5            41           1006             0            36            32
           3      0.304939            41            36            34            39            38             0         593.5            41           1007             0            36            32
           4      0.406526            41            36            34            39            38             0       595.333            41           1007             0            36            32

Took 0:00:00.576275
  Out [3]: Scan(number=15, name=loopscan, path=<no saving>)

```

#### Refinement

You want to specify in the configuration of this controller which
`default counters` are used for standard scan.  To do it you have to
provide a **property** `.counter_groups` which return a group
**default**

first you need to change your configuration file to:

```yaml
- plugin: bliss
  package: linux_sensors
  class: Sensor
  name: sensor
  default_counters: [Core_0,Core_1,Core_2,Core_3]
```

Manage the **default_counters** in the constructor of you controller

```python
    def __init__(self,name,config):
        self.name = name
        self.default_counters = [SamplingCounter(name,self)
                                 for name in config.get('default_counters',[])]
```

and add the **property** `.counter_groups`:

```python
    @autocomplete_property
    def counter_groups(self):
        if self.default_counters:
            return namespace({'default':self.default_counters})
        else:
            return namespace({})
```

!!! note
    bliss standard scan look first the **default** group in `.counter_groups` if exist.
    if not get the counters for `.counters` property. For this controller if
    the **default_counters** is not in the configuration file or empty, default scan
    will enable all counters.

Final file :

```python
import re
from gevent import subprocess
from bliss.common.measurement import SamplingCounter,counter_namespace,namespace
from bliss.common.utils import autocomplete_property

class Sensor:
    def __init__(self,name,config):
        self.name = name
        self.default_counters = [SamplingCounter(name,self)
                                 for name in config.get('default_counters',[])]
    @autocomplete_property
    def counters(self):
        counters = [SamplingCounter(name,self) for name in self._read_sensors()]
        return counter_namespace(counters)

    @autocomplete_property
    def counter_groups(self):
        if self.default_counters:
            return namespace({'default':self.default_counters})
        else:
            return namespace({})

    def read_all(self,*counters):
        sensors_values = self._read_sensors()
        return [sensors_values[cnt.name] for cnt in counters]

    def _read_sensors(self):
        p = subprocess.Popen("sensors",stdout=subprocess.PIPE)
        exp = re.compile(b"^(.+?): *[+-]?(\d+(\.\d*)?|\.\d+)")
        name2values = dict()
        name2nb = dict()
        for line in p.stdout.readlines():
            g = exp.match(line)
            if g:
                name,value = g.group(1),g.group(2)
                name = name.decode()
                name = name.replace(' ','_')
                nb = name2nb.setdefault(name,-1) + 1
                name2nb[name] = nb
                if nb :
                    name2values[f'{name}_{nb}'] = value
                else:
                    name2values[name] = value
        return name2values
```

In bliss console:

```
BLISS [1]: ls = config.get('linux_sensor')
BLISS [2]: ct(1,ls)

Wed Jul 31 14:20:03 2019

   dt[s] =          0.0 (         0.0/s)
  Core_0 =         36.0 (        36.0/s)
  Core_1 =         33.0 (        33.0/s)
  Core_2 =         41.0 (        41.0/s)
  Core_3 =         33.0 (        33.0/s)
  Out [2]: Scan(number=22, name=ct, path=<no saving>)

BLISS [3]: loopscan(5,.1,ls,save=False)

Scan 23 Wed Jul 31 14:20:21 2019 <no saving> default user = seb
loopscan 5 0.1

           #         dt[s]        Core_0        Core_1        Core_2        Core_3
           0             0            36            32            40            35
           1      0.101498            36            32            40            35
           2      0.203017            36            32            40            35
           3      0.304585            36            32            40            35
           4       0.40642            36            32            40            35

Took 0:00:00.541600
  Out [3]: Scan(number=23, name=loopscan, path=<no saving>)

BLISS [4]: loopscan(5,.1,ls.counters,save=False)

Scan 24 Wed Jul 31 14:20:30 2019 <no saving> default user = seb
loopscan 5 0.1

           #         dt[s]           CPU        Core_0        Core_1        Core_2        Core_3     Other_Fan   Other_Fan_1  Package_id_0  Processor_Fan        SODIMM      SODIMM_1      SODIMM_2
           0             0            39            36            33            36            34             0           587            40         998.75             0            35            32
           1      0.100959            39            36            33            36            34             0        586.75            40        999.625             0            35            32
           2       0.20197            40            36            33            36            34             0           586            40         1001.5             0            35            32
           3      0.303091            40            36            33            36            34             0       586.333            40        1003.11             0            35            32
           4      0.412361            40            36            33            36            34             0        587.75            40        1004.12             0            35            32

Took 0:00:00.576561
  Out [4]: Scan(number=24, name=loopscan, path=<no saving>)
```

##Raw Counters

Here is an example when you cannot use the counter type defined in bliss
core (**sampling counter**,**integrating counter**).

In this example the controller read the realtime *currency conversion*
and export is special counters, one per *currency conversion*.  Then
for standard scan each `counter` create two `AcquisitionChannel` one
**bid** and one **ask**

Configuration:

```yaml
- plugin: bliss
  package: currency
  class: Currency
  name: curr
```

python code:

```python
import requests
import json
import weakref
import numpy

from bliss.common.utils import autocomplete_property
from bliss.common.measurement import BaseCounter, namespace, counter_namespace
from bliss.scanning.chain import AcquisitionSlave, AcquisitionChannel


class Currency:
    def __init__(self,name,config):
        self.__name = name
        self.default_counters = [Counter(self,name)
                                 for name in config.get('default_counters',[])]
    @property
    def name(self):
        return self.__name

    @autocomplete_property
    def counters(self):
        return counter_namespace(Counter(self,name) for name in self.update().keys())

    @autocomplete_property
    def counter_groups(self):
        groups = {}
        if self.default_counters:
            groups['default'] = self.default_counters
        return namespace(groups)

    def update(self):
        r = requests.request(url="https://financialmodelingprep.com/api/v3/forex",method="GET")
        data = json.loads(r.content)
        return {conversion.pop('ticker').replace('/','_') : conversion
                for conversion in data['forexList']}


class Counter(BaseCounter):
    def __init__(self,financial,counter_name):
        self._controller = weakref.ref(financial)
        self._name = counter_name
    @autocomplete_property
    def controller(self):
        return self._controller()
    @property
    def name(self):
        return self._name

    def create_acquisition_device(self,scan_pars,**settings):
        return AcqDevice(self.controller,**scan_pars)

class AcqDevice(AcquisitionSlave):
    def __init__(self,financial,**scan_pars):
        AcquisitionSlave.__init__(self,financial,financial.name,
                                   npoints = scan_pars.get('npoints',1),
                                   prepare_once=True)
        self.counters = list()

    def add_counter(self, counter):
        channels = [AcquisitionChannel(self, f'{counter.name}:{k}', float, ())
                    for k in ['bid','ask']]
        self.channels.extend(channels)
        self.counters.append(counter)

    def prepare(self):
        pass

    def start(self):
        pass

    def trigger(self):
        values = self.device.update()
        values_dict = dict()
        for counter in self.counters:
            for k in ['bid','ask']:
                values_dict[f'{counter.name}:{k}'] = values[counter.name][k]
        self.channels.update(values_dict)

    def stop(self):
        pass
``` -->
