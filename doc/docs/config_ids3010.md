# IDS 3010 Attocube Interfermeter


!!!note
    To use `IDS3010`, the conda package `attocube_ids` (available on `esrf-bcu` conda
    channel) has to be installed.
    ```
    conda install attocube_ids -c esrf-bcu
    ```

```yaml
- name: ids
  plugin: generic
  module: interferometers.attocube_ids.ids3010
  class: Ids3010
  host: attopamu42.esrf.fr
```

Online info example:
```python
DEMO [2]: ids
 Out [2]: ids IDS3010
              Mode: measurement running
              Firmware Version: 1.7.0
              IP address: 172.24.165.249
              MAC address: 70:B3:D5:D7:60:D8
          ---------------  ------------  ------------  ------------
          AXES:            axis1         axis2         axis3
          ABS POS:         317284413666  317246704004  317563060257
          DISPLACEMENTS:   -500759       -484069       -589780
          CONTRAST:        836           772           0
          BASELINE:        0             0             0
          ---------------  ------------  ------------  ------------
```




## usage example


```python

DEMO [4]: ids
 Out [4]: IDS3010 : ids
           host: attopamu42.esrf.fr
           Mode: measurement running
           Firmware Version: 1.7.9
           IP address: 172.24.165.249
           MAC address: 70:B3:D5:D7:60:D8
           ---------------  ------------------  ------------------  ------------------
           AXES:            axis1               axis2               axis3
           ABS POS:         315,273,622,408 pm  315,320,782,246 pm  315,600,770,981 pm
           DISPLACEMENTS:   -331302             -311589             -274324
           CONTRAST:        nan                 nan                 nan
           BASELINE:        nan                 nan                 nan
           ---------------  ------------------  ------------------  ------------------

DEMO [6]: ids._set_optic_alignement_mode()
stopping measurement mode
wait for idle
.
idle
starting optics alignment mode
....
Alignment mode started in 2 seconds

DEMO [7]: ids
  Out [7]: IDS3010 : ids
           host: attopamu42.esrf.fr
           Mode: optics alignment running
           Firmware Version: 1.7.9
           IP address: 172.24.165.249
           MAC address: 70:B3:D5:D7:60:D8
           ---------  -----  -----  -----
           AXES:      axis1  axis2  axis3
           CONTRAST:  774    637    nan
           BASELINE:  0      54     nan
           ---------  -----  -----  -----

DEMO [9]: ct(1, ids)
   Fri Nov 19 14:17:31 2021: Scan(name=ct, path='not saved')
                    d1  =      nan           (     nan             /s)  ids
                    d2  =      nan           (     nan             /s)  ids
                    d3  =      nan           (     nan             /s)  ids
                    c1  =      760.000       (     760.000         /s)  ids
                    c2  =      626.000       (     626.000         /s)  ids
                    c3  =      nan           (     nan             /s)  ids

   Took 0:00:01.044249[s]

  Out [9]: Scan(name=ct, path='not saved')


DEMO [11]: ct(1, ids.counters.c1)
   Fri Nov 19 14:17:41 2021: Scan(name=ct, path='not saved')
                    c1  =      783.000       (     783.000         /s)  ids

   Took 0:00:01.024787[s]

  Out [11]: Scan(name=ct, path='not saved')


DEMO [12]: ids._set_measurement_mode()
stoping alignment mode
wait for idle
....
idle
starting measurement mode (~30s)
..........................
Alignment mode started in 26 seconds

DEMO [13]: ct(1, ids)
   Fri Nov 19 14:21:34 2021: Scan(name=ct, path='not saved')
                    d1  =  -336815.          ( -336815.            /s)  ids
                    d2  =  -306352.          ( -306352.            /s)  ids
                    d3  =  -307347.          ( -307347.            /s)  ids
                    c1  =      nan           (     nan             /s)  ids
                    c2  =      nan           (     nan             /s)  ids
                    c3  =      nan           (     nan             /s)  ids

   Took 0:00:01.086622[s]

  Out [13]: Scan(name=ct, path='not saved')
```

