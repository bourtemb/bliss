# CHANGELOG.md

## 0.4.0 (unreleased)

## 0.3.2

Bug fixes:
    - Python 3.10 and higher are not supported

## 0.3.1

Bug fixes:
    - Allow h5py 3.6 and higher

## 0.3.0

New features:
   - `h5py`-like API for
      - static HDF5 files (file content does not change while reading)
      - dynamic HDF5 files (file content changes while reading)

## 0.2.0

New features:
   - `RemoteNodeWalker` for non-gevent readers (gevent is still used in a subprocess)

## 0.1.2

Bug fixes:
   - pytango is optional

## 0.1.1

Bug fixes:
   - yaml config URL parsing fails for filename on Windows

## 0.1.0

New features:
   - Access to all data produced by Bliss acquisitions (Redis database 1)
   - Access to all public device settings (Redis database 0)
   - Access to beamline configuration (Beacon server)

The Redis related code has been extracted from the *Bliss* project without
changing the API.
