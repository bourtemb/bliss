# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Content related to color palette
"""

"""
Optimal qualitative colour palette from Anton Tsitsulin.
http://tsitsul.in/blog/coloropt/
"""
NORMAL_PALETTE_12 = [
    (87, 81, 212),
    (235, 171, 33),
    (176, 69, 0),
    (0, 197, 248),
    (207, 97, 230),
    (0, 166, 107),
    (184, 0, 87),
    (0, 138, 248),
    (0, 110, 0),
    (0, 186, 171),
    (255, 145, 133),
    (133, 133, 0),
]

"""
Optimal qualitative colour palette from Anton Tsitsulin.
http://tsitsul.in/blog/coloropt/
"""
NORMAL_PALETTE_6 = [
    (64, 83, 211),
    (221, 179, 16),
    (181, 29, 20),
    (0, 190, 255),
    (251, 73, 176),
    (0, 178, 93),
]
