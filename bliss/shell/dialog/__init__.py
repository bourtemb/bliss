from bliss.shell.dialog.common import measurementgroups, scans  # noqa: F401
from bliss.shell.dialog.controller import (  # noqa: F401
    transfocator,
    wago,
    white_beam_attenuator,
    lima_dialogs,
    multiplexer_dialogs,
)
from bliss.shell.dialog.controller.motors import slits  # noqa: F401
