import numpy
from typing import Optional, Callable, Any
from blissdata.data.scan import get_counter_names
from bliss import current_session, global_map
from bliss.common.protocols import Scannable
from bliss.common.types import _countable
from bliss.common.plot import display_motor
from bliss.scanning.scan import Scan
from bliss.scanning.scan_display import ScanDisplay
from bliss.common.utils import shorten_signature, typecheck
from bliss.common.plot import get_flint
from bliss.common.logtools import user_warning

"""
Alignment Helpers: cen peak com that interact with plotselect
and work outside of the context of a scan while interacting
with the last scan.
"""


@typecheck
def get_counter(counter_name: str):
    """
    Gets a counter instance from a counter name
    """
    for _counter in global_map.get_counters_iter():
        if _counter.fullname == counter_name:
            return _counter
    raise RuntimeError("Can't find the counter")


def get_selected_counter_name(counter=None) -> str:
    """
    Returns the name of the counter selected.

    That's one of the counters actually selected by `plotselect`. It does not
    mean the counter is actually displayed by Flint.

    Returns ONLY ONE counter.

    Raises RuntimeError if more than one counter is selected.

    Used to determine which counter to use for cen pic curs functions.
    """

    if not current_session.scans:
        raise RuntimeError("Scans list is empty!")
    scan_counter_names = set(get_counter_names(current_session.scans[-1]))

    scan_display = ScanDisplay()
    selected_counter_names = scan_display.displayed_channels
    alignment_counts = scan_counter_names.intersection(selected_counter_names)

    if not alignment_counts:
        # fall-back plan ... check if there is only one counter in the scan
        alignment_counts2 = {
            c
            for c in scan_counter_names
            if (":elapsed_time" not in c and ":epoch" not in c and "axis:" not in c)
        }
        if len(alignment_counts2) == 1:
            print(f"using {next(iter(alignment_counts2))} for calculation")
            alignment_counts = alignment_counts2
        else:
            raise RuntimeError(
                "No counter selected...\n"
                "Hints: Use flint or plotselect to define which counter to use for alignment"
            )
    elif len(alignment_counts) > 1:
        if counter is None:
            raise RuntimeError(
                "There is actually several counter selected (%s).\n"
                "Only one should be selected.\n"
                "Hints: Use flint or plotselect to define which counter to use for alignment"
                % alignment_counts
            )
        if counter.name in alignment_counts:
            return counter.name
        else:
            raise RuntimeError(
                f"Counter {counter.name} is not part of the last scan.\n"
            )
    counter_name = alignment_counts.pop()

    # Display warning on discrepancy with Flint
    flint = get_flint(mandatory=False, creation_allowed=False)
    if flint is not None:
        flint_selected_names = None
        try:
            plot_id = flint.get_default_live_scan_plot("curve")
            if plot_id is not None:
                flint_selected_names = flint.get_displayed_channels(plot_id)
        except Exception:
            pass
        else:
            if flint_selected_names is None or counter_name not in flint_selected_names:
                user_warning(
                    "The used counter name '%s' is not actually displayed in Flint",
                    counter_name,
                )
            elif counter_name in flint_selected_names and len(flint_selected_names) > 1:
                user_warning(
                    "The used counter name '%s' is not the only one displayed in Flint",
                    counter_name,
                )

    return counter_name


def last_scan_motors():
    """
    Return a list of motor used in the last scan.

    It includes direct motors (the one explicitly requested in the scan) and
    indirect motors used to compute the position of pseudo motors.
    """
    if not len(current_session.scans):
        raise RuntimeError("No scan available.")
    scan = current_session.scans[-1]

    return scan._get_data_axes(include_calc_reals=True)


def _scan_calc(func, counter=None, axis=None, scan=None, marker=True, goto=False):
    if counter is None:
        counter = get_counter(get_selected_counter_name())
    if scan is None:
        scan = current_session.scans[-1]
    if callable(func):
        res = scan.find_position(func, counter, axis=axis, return_axes=True)
        func = func.__name__  # for label managment
    else:
        res = getattr(scan, func)(counter, axis=axis, return_axes=True)
    if marker:
        clear_markers()
        for ax, value in res.items():
            display_motor(
                ax,
                scan=scan,
                position=value,
                label=func + "\n" + str(value),
                marker_id=func,
            )

            if ax in ["elapsed_time", "epoch"]:
                continue

            # display current position if in scan range
            scan_dat = scan.get_data()[ax]
            if (
                not goto
                and ax.position < numpy.max(scan_dat)
                and ax.position > numpy.min(scan_dat)
            ):
                display_motor(
                    ax,
                    scan=scan,
                    position=ax.position,
                    label="\n\ncurrent\n" + str(ax.position),
                    marker_id="current",
                )
    if goto:
        scan._goto_multimotors(res)
        display_motor(
            ax,
            scan=scan,
            position=ax.position,
            label="\n\ncurrent\n" + str(ax.position),
            marker_id="current",
        )
        return
    elif len(res) == 1:
        return next(iter(res.values()))
    else:
        return res


@typecheck
@shorten_signature(hidden_kwargs=[])
def fwhm(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return Full Width at Half of the Maximum of previous scan according to <counter>.
    If <counter> is not specified, use selected counter.

    Example: f = fwhm()
    """
    return _scan_calc("fwhm", counter=counter, axis=axis, scan=scan, marker=False)


@typecheck
@shorten_signature(hidden_kwargs=[])
def cen(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return the motor position corresponding to the center of the fwhm of the last scan.
    If <counter> is not specified, use selected counter.

    Example: cen(diode3)
    """
    return _scan_calc("cen", counter=counter, axis=axis, scan=scan)


@typecheck
def find_position(
    func: Callable[[Any, Any], float],
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    return _scan_calc(func, counter=counter, axis=axis, scan=scan)


@typecheck
@shorten_signature(hidden_kwargs=[])
def goto_cen(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return the motor position corresponding to the center of the fwhm of the last scan.
    Move scanned motor to this value.
    If <counter> is not specified, use selected counter.

    Example:
    """
    return _scan_calc("cen", counter=counter, axis=axis, scan=scan, goto=True)


@typecheck
@shorten_signature(hidden_kwargs=[])
def com(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return center of mass of last scan according to <counter>.
    If <counter> is not specified, use selected counter.

    Example: scan_com = com(diode2)
    """
    return _scan_calc("com", counter=counter, axis=axis, scan=scan)


@typecheck
@shorten_signature(hidden_kwargs=[])
def goto_com(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return center of mass of last scan according to <counter>.
    Move scanned motor to this value.
    If <counter> is not specified, use selected counter.

    Example: goto_com(diode2)
    """
    return _scan_calc("com", counter=counter, axis=axis, scan=scan, goto=True)


@typecheck
@shorten_signature(hidden_kwargs=[])
def peak(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return position of scanned motor at maximum of <counter> of last scan.
    If <counter> is not specified, use selected counter.

    Example: max_of_scan = peak()
    """
    return _scan_calc("peak", counter=counter, axis=axis, scan=scan)


@typecheck
@shorten_signature(hidden_kwargs=[])
def goto_peak(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return position of scanned motor at maximum of <counter> of last scan.
    Move scanned motor to this value.
    If <counter> is not specified, use selected counter.

    Example: goto_peak()
    """
    return _scan_calc("peak", counter=counter, axis=axis, scan=scan, goto=True)


@typecheck
@shorten_signature(hidden_kwargs=[])
def trough(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return position of scanned motor at minimum of <counter> of last scan.
    If <counter> is not specified, use selected counter.

    Example: min_of_scan = min()
    """
    return _scan_calc("trough", counter=counter, axis=axis, scan=scan)


@typecheck
@shorten_signature(hidden_kwargs=[])
def goto_min(
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    """
    Return position of scanned motor at minimum of <counter> of last scan.
    Move scanned motor to this value.
    If <counter> is not specified, use selected counter.

    Example: goto_min()
    """
    return _scan_calc("trough", counter=counter, axis=axis, scan=scan, goto=True)


@typecheck
def goto_custom(
    func: Callable[[Any, Any], float],
    counter: Optional[_countable] = None,
    axis: Optional[Scannable] = None,
    scan: Optional[Scan] = None,
):
    return _scan_calc(func, counter=counter, axis=axis, scan=scan, goto=True)


def where():
    """
    Draw a vertical line on the plot at current position of scanned motor.

    Example: where()
    """
    for axis in last_scan_motors():
        display_motor(
            axis, marker_id="current", label="\n\ncurrent\n" + str(axis.position)
        )


def clear_markers():
    for axis in last_scan_motors():
        display_motor(axis, marker_id="cen", position=numpy.nan)
        display_motor(axis, marker_id="peak", position=numpy.nan)
        display_motor(axis, marker_id="com", position=numpy.nan)
        display_motor(axis, marker_id="current", position=numpy.nan)
