"""
Controller and server for Xia's handel detector.

.. autosummary::
    :toctree:

    _cffi
    error
    interface
    mapping
    parser
    server
    stats
"""
