import { useState, useEffect } from "react";
import bliss_logo from "./assets/logo.svg";
import "./App.css";

import Spinner from "react-bootstrap/Spinner";
import DropdownButton from "react-bootstrap/DropdownButton";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import DropdownTreeSelect from "react-dropdown-tree-select";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Terminal } from "xterm";
import { FitAddon } from "xterm-addon-fit";
import { io } from "socket.io-client";
import "xterm/css/xterm.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-dropdown-tree-select/dist/styles.css";
import "@fortawesome/fontawesome-free/css/all.css";

function get_session_name() {
  let title = document.querySelector("title").textContent;
  let session_name = title.split("|")[1].trim();
  if (session_name === "default") {
    return "__DEFAULT__";
  } else {
    return session_name;
  }
}

const initialization_only = () => {};

function App() {
  const fitAddon = new FitAddon();
  const ws = io("ws://" + window.location.host);
  const [ready, set_ready] = useState(false);
  const [show_help, set_show_help] = useState(false);
  const [help_content, set_help_content] = useState({ __html: "" });
  const handle_close_help = () => {
    set_show_help(false);
  };
  const [active_mg_style, set_active_mg_style] = useState("hide");
  const [active_mg_data, set_active_mg_data] = useState([]);

  const handle_resize = () => {
    const session_name = get_session_name();
    const dims = fitAddon.proposeDimensions();
    fitAddon.fit();

    window.fetch("/term_size", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        w: dims.cols,
        h: dims.rows,
        session_name: session_name,
      }),
    });
  };

  useEffect(() => {
    const term_div = document.getElementById("terminal");
    const rows = parseInt(term_div.offsetHeight / 16);
    const cols = parseInt(term_div.offsetWidth / 9);

    const term = new Terminal({
      theme: { background: "white", foreground: "black", cursor: "black" },
      convertEol: true,
      rows: rows,
      cols: cols,
    });
    window.term = term; //TODO: remove this global
    term.loadAddon(fitAddon);

    term.open(term_div);
    const resize_observer = new ResizeObserver(handle_resize);
    resize_observer.observe(term_div);

    const session_name = get_session_name();

    ws.on("terminal_output", (data) => {
      typeof data === "string"
        ? term.write(data)
        : term.write(new Uint8Array(data));
    });
    term.onData((data) => {
      ws.emit("terminal_input", { input: data, session_name: session_name });
    });
    ws.emit("attach", { session_name: session_name, w: cols, h: rows });
    set_ready(false);
    term.options.disableStdin = true;
    ws.on("ready", () => {
      set_ready(true);
      term.options.disableStdin = false;
    });
    ws.on("show_help", (help_doc) => {
      set_help_content({ __html: help_doc });
      set_show_help(true);
    });
  }, [initialization_only]);

  const toggle_active_mg = (show) => {
    const session_name = get_session_name();
    if (show) {
      window
        .fetch("/session/" + session_name + "/active_mg")
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error("ACTIVE_MG: could not get data");
          }
        })
        .then((data) => {
          set_active_mg_style("");
          set_active_mg_data(data);
        })
        .catch((error) => {
          alert(error.message);
        });
    } else {
      set_active_mg_style("hide");
      if (window.active_mg_sel !== undefined) {
        set_ready(false);
        window.term.options.disableStdin = true;
        window
          .fetch("/session/" + session_name + "/active_mg", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(window.active_mg_sel),
          })
          .then((response) => {
            set_ready(true);
            window.term.options.disableStdin = false;
            if (!response.ok) {
              throw new Error("ACTIVE_MG: could not apply choices");
            }
          })
          .catch((error) => {
            alert(error.message);
          });
      }
    }
  };

  return (
    <div className="App">
      <Navbar bg="light">
        <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src={bliss_logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{" "}
            BLISS
          </Navbar.Brand>
        </Container>
        <Container>
          <DropdownButton
            title="ACTIVE_MG"
            autoClose={true}
            disabled={!ready}
            onToggle={(show, meta) => toggle_active_mg(show)}
          >
            <Spinner
              animation="border"
              role="status"
              className={active_mg_style === "" ? "hide" : ""}
            />
            <DropdownTreeSelect
              data={active_mg_data}
              onChange={(current_node, selected_nodes) => {
                window.active_mg_sel = selected_nodes;
              }}
              className={"active_mg " + active_mg_style}
            />
          </DropdownButton>
        </Container>
      </Navbar>
      <div id="terminal" className="Terminal"></div>
      <Modal show={show_help} onHide={handle_close_help}>
        <Modal.Header>
          <Modal.Title>Help</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div dangerouslySetInnerHTML={help_content} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handle_close_help}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default App;
