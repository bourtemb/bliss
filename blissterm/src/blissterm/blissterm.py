# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Bliss REPL (Read Eval Print Loop) as a Flask+xtermjs web application"""

# put bliss imports first, in order to ensure proper monkey-patching is done
# before importing flask and other dependencies
from bliss.common.greenlet_utils.asyncio_gevent import yield_future
from bliss.shell.cli.repl import BlissReplBase, cli
from bliss.shell.data.display import StepScanProgress
from bliss.scanning import scan as scan_module
from bliss.shell import log_utils
from bliss.shell import getval
import bliss as bliss_module

import os
import io
import builtins
import collections
import pprint
import types
import sys
import gevent
import pydoc
import threading
from collections import namedtuple, defaultdict
from contextlib import contextmanager
from pdb import Pdb
from typing import cast, TextIO
from flask import (
    Flask,
    request,
    redirect,
    jsonify,
    # stream_with_context,
    render_template,
)
from flask_socketio import SocketIO, join_room

from prompt_toolkit.data_structures import Size
from prompt_toolkit.shortcuts.prompt import PromptSession
from prompt_toolkit.input import create_pipe_input
from prompt_toolkit.output.vt100 import Vt100_Output
from prompt_toolkit.application import create_app_session, get_app_session
from tqdm import std as tqdm_std


class Debugger(Pdb):
    def __init__(self, server_repl, stdin, stdout):
        super().__init__(stdout=stdout)
        self._repl = server_repl
        self._current_line_execution = None

        self._input = stdin

        def custom_send_text(input, txt, orig_send_text=self._input.send_text):
            if txt == "\x03":
                if self._current_line_execution:
                    self._current_line_execution.kill(KeyboardInterrupt)
                    return
            return orig_send_text(txt)

        self._input.send_text = types.MethodType(custom_send_text, self._input)
        self.app = PromptSession(
            history=None,
            multiline=False,
            key_bindings=None,
            message="(Pdb) ",
        )

    def user_line(self, frame):
        self._repl._current_app = self.app
        return super().user_line(frame)

    def set_trace(self, frame=None):
        if frame is None:
            frame = sys._getframe().f_back
        return super().set_trace(frame)

    def set_continue(self):
        self._repl._current_app = self._repl.app
        return super().set_continue()

    def do_clear(self, arg):
        if not arg:
            # the original implementation asks confirmation with 'input',
            # let's make it simple for now
            self.message("Please specify breakpoints to clear")
        else:
            return super().do_clear(arg)

    def default(self, line):
        self._current_line_execution = gevent.spawn(super().default, line)
        return self._current_line_execution.get()

    def cmdloop(self):
        self.preloop()
        stop = None
        while not stop:
            if self.cmdqueue:
                line = self.cmdqueue.pop(0)
            else:
                # Run the prompt in a different thread.

                line = ""
                keyboard_interrupt = False
                watcher = gevent.get_hub().loop.async_()
                line_read_event = gevent.event.Event()
                watcher.start(line_read_event.set)

                def in_thread():
                    nonlocal line, keyboard_interrupt
                    try:
                        line = self.app.prompt()
                    except EOFError:
                        line = "EOF"
                    except KeyboardInterrupt:
                        keyboard_interrupt = True
                    finally:
                        watcher.send()

                th = threading.Thread(target=in_thread)
                th.start()

                try:
                    line_read_event.wait()
                finally:
                    watcher.stop()
                    watcher.close()
                    th.join()

                if line == "EOF":
                    self.do_quit("")
                    sys.settrace(None)
                    self.quitting = False
                    return
                elif keyboard_interrupt:
                    raise KeyboardInterrupt

            line = self.precmd(line)
            self.stdout.flush()
            stop = self.onecmd(line)
            self.stdout.flush()
            stop = self.postcmd(stop, line)
            self.stdout.flush()

        self.postloop()


class BlissServerReplStdout(io.BytesIO):
    def __init__(self, session_name, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._session_name = session_name

    @property
    def encoding(self):
        return "utf-8"

    @property
    def session_name(self):
        return self._session_name

    def write(self, data):
        if isinstance(data, str):
            data = bytes(data, "utf-8")
        # sys.__stdout__.write(f"IN SERVER STDouT {data}\n")
        return super().write(data)

    def flush(self):
        socketio.emit("terminal_output", self.getvalue(), to=self.session_name)
        self.seek(0)
        self.truncate()


class BlissServerRepl(BlissReplBase):
    default_term_size = Size(rows=40, columns=80)

    @staticmethod
    @contextmanager
    def ptpython_session(session_name):
        # PipeInput object, for sending input to the CLI
        # (This is something that we can use in the prompt_toolkit event loop,
        # but still write date in manually.)
        with create_pipe_input() as pipe_input:
            stdout = BlissServerReplStdout(session_name)
            output = Vt100_Output(
                cast(TextIO, stdout), lambda: BlissServerRepl.default_term_size
            )
            with create_app_session(pipe_input, output) as ptpython_session:
                yield ptpython_session

    # code here is inspired from ptpython/contrib/asyncssh_repl.py
    def __init__(self, *args, **kwargs):
        self._term_rows, self._term_cols = BlissServerRepl.default_term_size

        # sys.breakpointhook = self.breakpoint_hook

        super().__init__(*args, **kwargs)
        self.app.output._get_size = self._get_size

        self._debugger = None
        self._current_app = self.app

        # Disable open-in-editor and system prompt. Because it would run and
        # display these commands on the server side, rather than in the browser
        self.enable_open_in_editor = False
        self.enable_system_bindings = False

    def eval_greenlet(self, text):
        eval_g = super().eval_greenlet(text)
        eval_g.spawn_tree_locals["session_name"] = self.session_name
        return eval_g

    def initialize_session(self, early_log_info=None):
        init_g = gevent.spawn(super().initialize_session, early_log_info)
        init_g.spawn_tree_locals["session_name"] = self.session_name
        return init_g.get()

    def breakpoint_hook(self):
        self._current_app.output.flush()
        with BlissServerRepl.ptpython_session(self.session_name) as ptpython_session:
            self._debugger = Debugger(
                self, ptpython_session.input, ptpython_session.output
            )
            self._debugger.app.output._get_size = self._get_size
            self._current_app = self._debugger.app
            # print = functools.partial(_orig_print, file=ptpython_session.output)
            try:
                self._debugger.set_trace(sys._getframe().f_back)
            finally:
                self._current_app = self.app

    def send_ctrl_c(self):
        if self._current_app is self.app and self._current_eval_g:
            self._current_eval_g.kill(KeyboardInterrupt)

    def send_text(self, text):
        self._current_app.input.send_text(text)

    def _get_size(self) -> Size:
        """
        Callable that returns the current `Size`, required by Vt100_Output.
        """
        return Size(rows=self._term_rows, columns=self._term_cols)

    def terminal_size_changed(self, width, height):
        """
        When the terminal size changes, report back to CLI.
        """
        self._term_rows = height
        self._term_cols = width

        def screen_shape(fp):
            return width - 1, height

        tqdm_std._screen_shape_wrapper = lambda: screen_shape
        self.app._on_resize()


app = None
Repl = namedtuple("Repl", ["cmd_line_i", "cli_greenlet"])
repls = {}
init_locks = defaultdict(gevent.lock.Semaphore)


def get_session_name_from_current_greenlet():
    current_greenlet = gevent.getcurrent()
    # sys.__stdout__.write(f"CURRENT GREENLET={repr(current_greenlet)}\n")
    if isinstance(current_greenlet, gevent.Greenlet):
        # sys.__stdout__.write(f"  session name = {repr(session_name)}\n")
        return current_greenlet.spawn_tree_locals.get("session_name")


def get_output():
    session_name = get_session_name_from_current_greenlet()
    if session_name:
        try:
            return repls[session_name].cmd_line_i.app.output
        except KeyError:
            return get_app_session().output


_orig_print = builtins.print


def _blissterm_print(*args, **kwargs):
    if "file" not in kwargs:
        output = get_output()
        if output:
            kwargs["file"] = output
            # sys.__stdout__.write(f"  output = {repr(args)} / {kwargs['file']}\n")
    return _orig_print(*args, **kwargs)


builtins.print = _blissterm_print


def scan_progress():
    return StepScanProgress(output=get_output())


scan_module._scan_progress_class = scan_progress


def get_current_session():
    session_name = get_session_name_from_current_greenlet()
    # sys.__stdout__.write(f"SESS={session_name}\n")
    if session_name:
        return bliss_module._sessions[session_name]


bliss_module._get_current_session = get_current_session


def get_cmd_line(session_name):
    # sys.__stdout__.write(f"GET CMDLINE {session_name} , {repr(repls)}\n")
    with init_locks[session_name]:
        try:
            return repls[session_name].cmd_line_i
        except KeyError:
            ready_event = gevent.event.Event()
            cmd_line_i = None

            def show_help(thing):
                # format web page with pydoc, the code below is similar to genuine 'help' builtin
                obj, name = pydoc.resolve(thing, 0)  # forceload=0
                page = pydoc.html.page(
                    pydoc.describe(obj), pydoc.html.document(obj, name)
                )
                socketio.emit("show_help", page, to=session_name)

            def run_cmd_line():
                nonlocal cmd_line_i
                with BlissServerRepl.ptpython_session(session_name):
                    cmd_line_i = cli(
                        BlissServerRepl,
                        session_name=session_name,
                        style="xcode",  # solarized-dark",
                    )
                    cmd_line_i.bliss_session.env_dict["help"] = show_help
                    # cmd_line_i.bliss_session.env_dict["print"] = functools.partial(
                    #    _orig_print, file=cmd_line_i.app.output
                    # )
                    cmd_line_i.bliss_session.env_dict[
                        "breakpoint"
                    ] = cmd_line_i.breakpoint_hook

                    ready_event.set()

                    yield_future(cmd_line_i.run_async())

            cli_greenlet = gevent.spawn(run_cmd_line)

            ready_event.wait()

            repls[session_name] = Repl(cmd_line_i, cli_greenlet)

            return cmd_line_i


def getval_prompt(message, validator=None):
    session_name = get_session_name_from_current_greenlet()
    cmd_line_i = get_cmd_line(session_name)

    with BlissServerRepl.ptpython_session(session_name):
        session = getval.BlissPromptSession()
        repl = cmd_line_i.app
        cmd_line_i._current_app = session

        try:
            return yield_future(
                session.prompt_async(
                    message,
                    validator=validator,
                    handle_sigint=False,
                )
            )
        except getval._GetvalKeyboardInterrupt:
            raise KeyboardInterrupt
        finally:
            cmd_line_i._current_app = repl


getval.bliss_prompt = getval_prompt


app = Flask(
    __name__,
    static_url_path="",
    static_folder=os.path.join(os.path.dirname(__file__), "static"),
    template_folder=os.path.join(os.path.dirname(__file__), "static"),
)
app.config["SECRET_KEY"] = "blissisthesecret"
socketio = SocketIO(app)


@app.route("/term_size", methods=["POST"])
def set_terminal_size():
    # report web terminal size to ptpython
    session_name = request.json["session_name"]
    w = request.json["w"]
    h = request.json["h"]
    try:
        cmd_line_i = repls[session_name].cmd_line_i
    except KeyError:
        pass
    else:
        cmd_line_i.terminal_size_changed(w, h)
    return jsonify(success=True)


@app.route("/session/<session_name>/active_mg")
def get_active_mg_data(session_name):
    try:
        cmd_line_i = repls[session_name].cmd_line_i
    except KeyError:
        return "Session is not active", 400
    else:
        mg = cmd_line_i.bliss_session.active_mg
        grouped = collections.defaultdict(lambda: {"children": []})
        for cnt in sorted(mg.available):
            cnt_prefix, _, cnt_name = cnt.partition(":")
            grouped[cnt_prefix]["label"] = cnt_prefix
            grouped[cnt_prefix]["children"].append(
                {
                    "label": cnt_name,
                    "tagLabel": cnt,
                    "children": [],
                    "checked": cnt in mg.enabled,
                }
            )
            grouped[cnt_prefix]["checked"] = all(
                child["checked"] for child in grouped[cnt_prefix]["children"]
            )
    pprint.pprint(list(grouped.values()))
    return list(grouped.values()), 200


@app.route("/session/<session_name>/active_mg", methods=["POST"])
def set_active_mg_counters(session_name):
    try:
        cmd_line_i = repls[session_name].cmd_line_i
    except KeyError:
        return "Session is not active", 400
    else:
        data = request.json
        cmd_line_i.bliss_session.active_mg.disable("*")
        for item in data:
            if item["checked"]:
                try:
                    cmd_line_i.bliss_session.active_mg.enable(item["tagLabel"])
                except KeyError:
                    # not a child ; enable underlying counters
                    cmd_line_i.bliss_session.active_mg.enable(item["label"] + ":*")

    return jsonify(success=True)


@app.route("/")
@app.route("/session")
def redirect_default_session():
    return redirect("/session/default")


@app.route("/session/<session_name>")
def initiate_session(session_name):
    return render_template("index.html", session_name=session_name)


@socketio.on("attach")
def xterm_connected(data):
    session_name = data["session_name"]
    join_room(session_name)
    cmd_line_i = get_cmd_line(data["session_name"])
    cmd_line_i.terminal_size_changed(data["w"], data["h"])
    socketio.emit("ready", to=session_name)


@socketio.on("terminal_input")
def xterm_receive_terminal_input(data):
    cmd_line_i = get_cmd_line(data["session_name"])
    cmd_line_i.send_text(data["input"])
    if data["input"] == "\x03":
        cmd_line_i.send_ctrl_c()


# SSE EXAMPLE BELOW:
# @app.route("/session/<session_name>/help")
# def display_help(session_name):
#    help_queue = repls[session_name].help_queue
#
#    def help_message_stream():
#        while True:
#            thing = help_queue.get()
#            # format web page with pydoc, the code below is similar to genuine 'help' builtin
#            obj, name = pydoc.resolve(thing, 0)  # forceload=0
#            page = pydoc.html.page(pydoc.describe(obj), pydoc.html.document(obj, name))
#            # the data has to be sent back to the client, using SSE protocol
#            for line in str(page).split("\n"):
#                yield f"data: {line}\n"
#            yield f"data: \n\n"
#
#    return app.response_class(
#        stream_with_context(help_message_stream()),
#        status=200,
#        mimetype="text/plain",
#        content_type="text/event-stream",
#    )


def main():
    with log_utils.filter_warnings():
        socketio.run(app)


if __name__ == "__main__":
    main()
