import enum
import gevent

from bliss.config.beacon_object import BeaconObject, EnumProperty
from bliss.testutils.event_utils import EventListener


class FakeEnum(enum.Enum):
    UNKNOWN = "UNKNOWN"
    ON = "ON"
    OFF = "OFF"


class Fake(BeaconObject):
    state = EnumProperty("state", default=FakeEnum.UNKNOWN, enum_type=FakeEnum)


def test_constructor(beacon):
    config = {"name": "fake1"}
    fake = Fake(config)
    assert fake.state == FakeEnum.UNKNOWN


def test_setter(beacon):
    config = {"name": "fake2"}
    fake = Fake(config)
    fake.state = FakeEnum.ON
    assert fake.state == FakeEnum.ON


def test_event(beacon):
    config = {"name": "fake3"}
    fake = Fake(config)
    listener = EventListener()

    with listener.listening(fake, "state") as state_changed_event:
        fake.state = FakeEnum.ON
        assert fake.state == FakeEnum.ON
        with gevent.Timeout(3):
            state_changed_event.wait()  # ensure change event is received

    listener = EventListener()
    with gevent.Timeout(3):
        with listener.listening(fake, "state", wait_event=True):
            fake.state = FakeEnum.OFF

    assert listener.event_count == 1
    assert listener.last_value == FakeEnum.OFF
