import gevent

from bliss.config.beacon_object import BeaconObject
from bliss.common import event
from bliss.testutils.event_utils import EventListener


class Fake(BeaconObject):
    def __init__(self, name, config):
        super(Fake, self).__init__(config, name)

    name = BeaconObject.config_getter("name")

    devices = BeaconObject.config_obj_list_property_setting("devices")


def test_empty_config(beacon):
    """
    Test an empty object
    """
    fakeclass = {"class": "Fake", "package": __name__}
    beacon.get_config("fake1").update(fakeclass)

    fake1 = beacon.get("fake1")
    assert fake1.devices == tuple()


def test_devices_config(beacon):
    """
    Test an object configured with a list of objects
    """
    fakeclass1 = {"class": "Fake", "package": __name__, "devices": ["$fake2", "$fake3"]}
    beacon.get_config("fake1").update(fakeclass1)
    fakeclass = {"class": "Fake", "package": __name__}
    beacon.get_config("fake2").update(fakeclass)
    beacon.get_config("fake3").update(fakeclass)

    fake1 = beacon.get("fake1")
    assert isinstance(fake1.devices, tuple)
    devices = tuple([o.name for o in fake1.devices])
    assert devices == ("fake2", "fake3")


def test_setter(beacon):
    """
    Test to set a new list of objects
    """
    fakeclass = {"class": "Fake", "package": __name__}
    beacon.get_config("fake1").update(fakeclass)
    beacon.get_config("fake2").update(fakeclass)
    beacon.get_config("fake3").update(fakeclass)

    fake1 = beacon.get("fake1")
    fake2 = beacon.get("fake2")
    fake3 = beacon.get("fake3")

    fake1.devices = [fake2, fake3]
    assert fake1.devices == (fake2, fake3)


def test_event(beacon):
    """
    Test that events is emitted with setting a list of objects
    """
    fakeclass = {"class": "Fake", "package": __name__}
    beacon.get_config("fake1").update(fakeclass)
    beacon.get_config("fake2").update(fakeclass)
    beacon.get_config("fake3").update(fakeclass)

    fake1 = beacon.get("fake1")
    fake2 = beacon.get("fake2")
    fake3 = beacon.get("fake3")

    fake1.devices = [fake2]
    gevent.sleep(1)

    listener = EventListener()
    try:
        event.connect(fake1, "devices", listener)
        fake1.devices = [fake2, fake3]
        # Wait for event received
        gevent.sleep(1)
    finally:
        event.disconnect(fake1, "devices", listener)

    assert listener.event_count == 1
    # Events are received as serialized version
    assert listener.last_value == ("fake2", "fake3")
