# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest
import mock
from blissdata.client import configure_with_beacon_address
from blissdata.settings import SimpleSetting


def create_simple_setting():
    setting = SimpleSetting("my_key")
    setting.set("some_value")
    assert setting.get() == "some_value"


# prevent BEACON_HOST to be set by session scope fixtures
@mock.patch.dict(os.environ, clear=True)
def test_blissdata_not_configured():
    err_msg = "Blissdata configuration from BEACON_HOST failed"

    with pytest.raises(RuntimeError, match=err_msg):
        create_simple_setting()

    # set an invalid address
    os.environ["BEACON_HOST"] = "localhost:0"

    with pytest.raises(RuntimeError, match=err_msg):
        create_simple_setting()


def test_blissdata_manual_config(beacon_host_port):
    # without beacon fixture, one should give blissdata the beacon address,
    # so it can request URLs to Redis databases
    host, port = beacon_host_port
    configure_with_beacon_address(host, port)
    create_simple_setting()


def test_blissdata_default_config(beacon):
    # beacon fixture configures blissdata as Bliss would do.
    # no need to give it a beacon address by hand
    create_simple_setting()
