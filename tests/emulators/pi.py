# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
PI (Physik Instrumente) emulator helper classes

To create a pure PI device use the following configuration as
a starting point:

.. code-block:: yaml

    name: my_emulator
    devices:
        - class: PI_E712
          module: pi
          model: 6CD         # optional (default is 3CD)
          transports:
              - type: tcp
                url: :25000

To start the server you can do something like::

    $ python -m tests.emulators.emulator my_emulator

A simple *nc* client can be used to connect to the instrument::

    $ nc 0 25000
    *idn?
    (c)2013 Physik Instrumente(PI) Karlsruhe, E-712.6CD, 0, 0.1.0

"""

import weakref

from .scpi import SCPI
from bliss.comm.scpi import Commands, FuncCmd, IntCmdRO, IDNCmd, ErrCmd


class PI(SCPI):
    def __init__(self, *args, **kwargs):
        super(PI, self).__init__(*args, **kwargs)
        self._manufacturer = "(c)2013 Physik Instrumente(PI) Karlsruhe"


class PIAxis(object):
    def __init__(self, pi, channel=None):
        self._pi = weakref.ref(pi)
        self._channel = channel
        self.__pos = 0.0
        self.__set_mov = 0.0
        self.__set_sva = 0.0
        self.__vel = 100.0
        self.__vol = 0.0
        self.__svo = 0
        self.__ont = 1
        self.__cto = 3, 3, 1, 5, 0, 1, 6, 100, 1, 7, 0

    @property
    def pos(self):
        return self.__pos

    @property
    def mov(self):
        return self.__set_mov

    @mov.setter
    def mov(self, new_pos):
        self.__pos = new_pos
        self.__set_mov = new_pos

    @property
    def sva(self):
        return self.__set_sva

    @sva.setter
    def sva(self, new_pos):
        self.__pos = new_pos
        self.__set_sva = new_pos

    @property
    def vel(self):
        return self.__vel

    @vel.setter
    def vel(self, new_vel):
        self.__vel = new_vel

    @property
    def vol(self):
        return self.__vol

    @property
    def svo(self):
        return self.__svo

    @svo.setter
    def svo(self, yesno):
        self.__svo = yesno

    @property
    def ont(self):
        return self.__ont

    def cto(self, *args):
        self.__cto = args


class PI_E712(PI):
    """
    Modular digital controller for multi- axis piezo nanopositioning systems
    with capacitive sensors
    """

    # special messages are sent without '\r'
    special_messages = set([chr(9)])  # Get Wave Generator Status

    def __init__(self, name, axes=None, **opts):
        model = opts.pop("model", "E-712.3CD")
        opts["commands"] = Commands(
            {
                "ERR": ErrCmd(),
                "POS": FuncCmd(),
                "VEL": FuncCmd(),
                "MOV": FuncCmd(),
                "SVA": FuncCmd(),
                "SVO": FuncCmd(),
                "ONT": FuncCmd(),
                "CTO": FuncCmd(),
                "CCL": IntCmdRO(value=0),
                "*IDN": IDNCmd(),
                "IFC": FuncCmd(),
            }
        )
        super(PI_E712, self).__init__(name, **opts)
        self._model = model
        axes_dict = {}
        if axes is None:
            n = 3 if "3C" in model else 6
            axes = [dict(channel=c) for c in range(1, n + 1)]
        for axis in axes:
            axes_dict[axis["channel"]] = PIAxis(self, **axis)
        self._axes = axes_dict
        for k, v in list(opts.items()):
            setattr(self, "_" + k, v)

    def wave_generator_status(self):
        return 0

    def handle_special_msg(self, line):
        if line == chr(9):
            return str(self.wave_generator_status())
        else:
            raise ValueError(f"Unknown special message '{line}'")

    def err(self):
        return "0"

    def pos(self, channel):
        return f"{channel}={self._axes[int(channel)].pos}"

    def vel(self, is_query, channel, new_vel=None):
        axis = self._axes[int(channel)]
        if is_query:
            return f"{channel}={axis.vel}"
        axis.vel = new_vel

    def mov(self, is_query, channel, new_pos=None):
        axis = self._axes[int(channel)]
        if is_query:
            return f"{channel}={axis.mov}"
        axis.mov = new_pos

    def sva(self, is_query, channel, new_pos=None):
        axis = self._axes[int(channel)]
        if is_query:
            return f"{channel}={axis.sva}"
        axis.sva = new_pos

    def svo(self, is_query, channel, yesno=None):
        axis = self._axes[int(channel)]
        if is_query:
            return f"{channel}={axis.svo}"
        axis.svo = yesno

    def ont(self, channel):
        return f"{channel}={self._axes[int(channel)].ont}"

    def cto(self, channel, trig_mode, min_threshold, max_threshold, polarity):
        self._axes[int(channel)].cto(trig_mode, min_threshold, max_threshold, polarity)

    def ifc(self, is_query, *args):
        if is_query:
            result = []
            for arg in args:
                if arg == "IPADR":
                    result.append("IPADR=123.45.67.89")
                elif arg == "MACADR":
                    result.append("MACADR=aa:bb:cc:dd:ee:ff")
                elif arg == "IPSTART":
                    result.append("IPSTART=0")  # 0->STATIC 1->DHCP
            return "\n".join(result)
        else:
            raise NotImplementedError
