"""Testing manager module."""

from bliss.flint.manager.manager import ManageMainBehaviours
from bliss.flint.model import flint_model
from bliss.flint.model import plot_model
from bliss.flint.model import plot_item_model
from bliss.flint.widgets.curve_plot import CurvePlotWidget
from bliss.flint.helper import scan_info_helper
from . import scan_examples


def test_scatter_plot__no_user_selection(local_flint):
    """
    Test plot state with consecutive scans and no user selection in between

    We expect the scan to be displayed as specified by the scan
    """
    flint = flint_model.FlintState()
    workspace = flint_model.Workspace()
    flint.setWorkspace(workspace)
    widget = CurvePlotWidget()
    workspace.addWidget(widget)

    manager = ManageMainBehaviours()
    manager.setFlintModel(flint)

    mesh_scan_info = scan_examples.create_mesh_scan_info("diode:diode1")
    scan = scan_info_helper.create_scan_model(mesh_scan_info)
    plots = scan_info_helper.create_plot_model(mesh_scan_info, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.ScatterPlot)][0]
    manager.updateWidgetWithPlot(widget, scan, plot, useDefaultPlot=False)
    model = widget.plotModel()
    assert len(model.items()) == 1

    mesh_scan_info2 = scan_examples.create_mesh_scan_info("diode:diode2")
    scan = scan_info_helper.create_scan_model(mesh_scan_info2)
    plots = scan_info_helper.create_plot_model(mesh_scan_info2, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.ScatterPlot)][0]
    manager.updateWidgetWithPlot(widget, scan, plot, useDefaultPlot=False)

    model = widget.plotModel()
    assert len(model.items()) == 1
    item = model.items()[0]
    assert item.xChannel().name() == "axis:sx"
    assert item.yChannel().name() == "axis:sy"
    assert item.valueChannel().name() == "diode:diode2"


def test_scatter_plot__user_selection(local_flint):
    """
    Test plot state with consecutive scans and a user selection in between

    We expect the user selection to be restored (including X and Y axis)
    """
    flint = flint_model.FlintState()
    workspace = flint_model.Workspace()
    flint.setWorkspace(workspace)
    widget = CurvePlotWidget()
    workspace.addWidget(widget)

    manager = ManageMainBehaviours()
    manager.setFlintModel(flint)

    mesh_scan_info = scan_examples.create_mesh_scan_info("diode:diode1")
    scan = scan_info_helper.create_scan_model(mesh_scan_info)
    plots = scan_info_helper.create_plot_model(mesh_scan_info, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.ScatterPlot)][0]
    manager.updateWidgetWithPlot(widget, scan, plot, useDefaultPlot=False)
    model = widget.plotModel()
    assert len(model.items()) == 1

    # user selection
    with plot.transaction():
        item = plot.items()[0]
        item.setXChannel(plot_model.ChannelRef(plot, "axis:sxenc"))
        item.setYChannel(plot_model.ChannelRef(plot, "axis:syenc"))
        item.setValueChannel(plot_model.ChannelRef(plot, "diode:diode2"))
    plot.tagUserEditTime()

    mesh_scan_info2 = scan_examples.create_mesh_scan_info("diode:diode1")
    scan = scan_info_helper.create_scan_model(mesh_scan_info2)
    plots = scan_info_helper.create_plot_model(mesh_scan_info2, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.ScatterPlot)][0]
    manager.updateWidgetWithPlot(widget, scan, plot, useDefaultPlot=False)

    model = widget.plotModel()
    assert len(model.items()) == 1
    item = model.items()[0]
    assert item.xChannel().name() == "axis:sxenc"
    assert item.yChannel().name() == "axis:syenc"
    assert item.valueChannel().name() == "diode:diode2"


def test_image_plot_with_new_roi(local_flint):
    """Test the resulted image plot when a new ROI is part of the scan.

    We expect:
    - The previous ROI to still use the same config
    - The new ROI to be displayed.
    """
    flint = flint_model.FlintState()
    workspace = flint_model.Workspace()
    flint.setWorkspace(workspace)
    widget = CurvePlotWidget()
    workspace.addWidget(widget)

    manager = ManageMainBehaviours()
    manager.setFlintModel(flint)

    scan_info1 = scan_examples.create_lima_scan_info(include_roi2=False)
    scan = scan_info_helper.create_scan_model(scan_info1)
    plots = scan_info_helper.create_plot_model(scan_info1, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.ImagePlot)][0]
    manager.updateWidgetWithPlot(widget, scan, plot, useDefaultPlot=True)

    plotModel = widget.plotModel()
    assert len(plotModel.items()) == 2  # image + ROI

    roiItem = [i for i in plotModel.items() if isinstance(i, plot_item_model.RoiItem)][
        0
    ]
    roiItem.setVisible(False)

    scan_info2 = scan_examples.create_lima_scan_info(include_roi2=True)
    scan = scan_info_helper.create_scan_model(scan_info2)
    plots = scan_info_helper.create_plot_model(scan_info2, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.ImagePlot)][0]
    manager.updateWidgetWithPlot(widget, scan, plot, useDefaultPlot=True)

    plotModel = widget.plotModel()
    assert len(plotModel.items()) == 3  # image + ROI * 2

    roiItems = [i for i in plotModel.items() if isinstance(i, plot_item_model.RoiItem)]
    rois = {r.name(): r.isVisible() for r in roiItems}
    assert rois == {"roi1": False, "roi2": True}
