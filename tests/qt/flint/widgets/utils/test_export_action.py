"""Testing for profile action"""

import numpy
from bliss.flint.model import plot_item_model
from bliss.flint.model import plot_model
from bliss.flint.model import scan_model
from bliss.flint.filters.derivative import DerivativeItem
from bliss.flint.filters.max import MaxCurveItem
from bliss.flint.widgets.utils import export_action


def test_export_csv(tmp_path):
    plot = plot_item_model.CurvePlot()
    plot.setScansStored(True)

    item = plot_item_model.CurveItem(plot)
    item.setXChannel(plot_model.ChannelRef(None, "x"))
    item.setYChannel(plot_model.ChannelRef(None, "y"))
    plot.addItem(item)

    item2 = DerivativeItem(plot)
    item2.setYAxis("right")
    item2.setSource(item)
    plot.addItem(item2)

    item3 = MaxCurveItem(plot)
    item3.setSource(item2)

    scan = scan_model.Scan()
    device = scan_model.Device(scan)
    channel = scan_model.Channel(device)
    channel.setType(scan_model.ChannelType.COUNTER)
    channel.setName("y")
    data = scan_model.Data(scan, numpy.arange(6) * 10)
    channel.setData(data)
    channel = scan_model.Channel(device)
    channel.setName("x")
    channel.setType(scan_model.ChannelType.COUNTER)
    data = scan_model.Data(scan, numpy.arange(5))
    channel.setData(data)
    scan.seal()

    filename = tmp_path / "data.csv"
    export_action.export_plot_as_csv(plot, scan, filename=filename)

    with open(filename, "rt") as f:
        data = f.read()
    assert data.startswith("x\ty\n")
    assert "2\t20\n" in data
    assert data.endswith("4\t40\n")
