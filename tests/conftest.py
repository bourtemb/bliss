# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import sys
import shutil
from collections import namedtuple
import atexit
import gc
from unittest.mock import MagicMock
import gevent
from gevent import Greenlet
import subprocess
import logging
import pytest
import redis
import redis.connection
from contextlib import contextmanager
import weakref
import socket
from pathlib import Path

import bliss
from bliss import global_map, global_log
from bliss.common import session as session_module
from bliss.common.utils import get_open_ports, rm_tree, copy_tree
from bliss.common import logtools
from bliss.config import static
from bliss.config.conductor import client
from bliss.config.conductor import connection
from bliss.config.conductor.client import (
    get_default_connection,
    get_default_redis_connection_manager,
)
from bliss.controllers.lima.roi import Roi
from bliss.controllers.wago.wago import ModulesConfig
from bliss.controllers.wago.emulator import WagoEmulator
from bliss.controllers import simulation_diode
from bliss.controllers import tango_attr_as_counter
from bliss.flint.client import proxy
from bliss.common import plot
from bliss.common.tango import DeviceProxy, ApiUtil, DevState
from bliss.tango.clients.utils import wait_tango_db
from bliss.shell.cli.repl import BlissRepl
from bliss.shell.log_utils import logging_startup
from bliss.scanning import scan_meta
from bliss.testutils.process_utils import start_tango_server, wait_terminate
from bliss.testutils.comm_utils import wait_tcp_online
from blissdata import client as blissdata_client
from bliss.icat.client import DatasetId

BLISS = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BEACON = [sys.executable, "-m", "bliss.config.conductor.server"]
BEACON_DB_PATH = os.path.join(BLISS, "tests", "test_configuration")
IMAGES_PATH = os.path.join(BLISS, "tests", "images")


def eprint(*args):
    print(*args, file=sys.stderr, flush=True)


@pytest.fixture(autouse=True)
def clean_louie():
    import louie.dispatcher as disp

    disp.connections = {}
    disp.senders = {}
    disp.senders_back = {}
    disp.plugins = []
    yield disp
    assert disp.connections == {}
    assert disp.senders == {}
    assert disp.senders_back == {}
    assert disp.plugins == []
    disp.reset()


class ResourcesContext:
    """
    This context ensure that every resource created during its execution
    are properly released.

    If a resource is not released at the exit, a warning is displayed,
    and it tries to release it.

    It is not concurrency safe.
    """

    def __init__(self, release, is_released, *resource_classes):
        self.resource_classes = resource_classes
        self.is_released = is_released
        self.release = release
        self.resources_before = weakref.WeakSet()
        self.all_resources_released = None

    def _iter_referenced_resources(self):
        for ob in gc.get_objects():
            try:
                if not isinstance(ob, self.resource_classes):
                    continue
            except ReferenceError:
                continue
            yield ob

    def __enter__(self):
        self.resources_before.clear()
        self.all_resources_released = None
        for ob in self._iter_referenced_resources():
            self.resources_before.add(ob)
        return self

    def resource_repr(self, ob):
        return repr(ob)

    def __exit__(self, exc_type, exc_val, exc_tb):
        resources = []
        for ob in self._iter_referenced_resources():
            if ob in self.resources_before:
                continue
            if not self.is_released(ob):
                eprint(f"Resource not released: {self.resource_repr(ob)}")
            resources.append(ob)

        self.resources_before.clear()
        self.all_resources_released = all(self.is_released(r) for r in resources)
        if not resources:
            return
        err_msg = f"Resources {self.resource_classes} cannot be released"
        with gevent.Timeout(10, RuntimeError(err_msg)):
            for r in resources:
                self.release(r)


class GreenletsContext(ResourcesContext):
    def __init__(self):
        super().__init__(lambda glt: glt.kill(), lambda glt: glt.ready(), Greenlet)


class SocketsContext(ResourcesContext):
    def __init__(self):
        super().__init__(
            lambda sock: sock.close(), lambda sock: sock.fileno() == -1, socket.socket
        )

    def resource_repr(self, sock):
        try:
            return f"{repr(sock)} connected to {sock.getpeername()}"
        except Exception:
            return f"{repr(sock)} not connected"


class RedisConnectionContext(ResourcesContext):
    def __init__(self):
        super().__init__(
            lambda conn: conn.disconnect(),
            lambda conn: conn._sock.fileno() == -1,
            redis.connection.Connection,
        )

    def resource_repr(self, conn):
        return f"{repr(conn)} connected to {conn._sock.getpeername()}"


@pytest.fixture(autouse=True)
def clean_gevent():
    """
    Context manager to check that greenlets are properly released during a test.

    It is not concurrency safe. The global context is used to
    check available greenlets.

    If the fixture is used as the last argument if will only test the greenlets
    creating during the test.

    .. code-block:: python

        def test_a(fixture_a, fixture_b, clean_gevent):
            ...

    If the fixture is used as the first argument if will also test greenlets
    created by sub fixtures.

    .. code-block:: python

        def test_b(clean_gevent, fixture_a, fixture_b):
            ...
    """
    d = {"end-check": True}
    with GreenletsContext() as context:
        yield d
    end_check = d.get("end-check")
    if end_check:
        assert context.all_resources_released


@pytest.fixture
def clean_socket():
    """
    Context manager to check that sockets are properly closed during a test.

    It is not concurrency safe. The global context is used to
    check available sockets.

    If the fixture is used as the last argument if will only test the sockets
    creating during the test.

    .. code-block:: python

        def test_a(fixture_a, fixture_b, clean_gevent):
            ...

    If the fixture is used as the first argument if will also test sockets
    created by sub fixtures.

    .. code-block:: python

        def test_b(clean_socket, fixture_a, fixture_b):
            ...
    """
    d = {"end-check": True}
    with SocketsContext() as context:
        yield d
    end_check = d.get("end-check")
    if end_check:
        assert context.all_resources_released


@pytest.fixture(autouse=True)
def clean_globals():
    orig_excepthook = sys.excepthook
    yield
    sys.excepthook = orig_excepthook
    global_log.clear()
    global_map.clear()
    # reset module-level globals
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_INTEGRATING_CONTROLLER = None
    bliss._BLISS_SHELL_MODE = False
    session_module.sessions.clear()
    scan_meta.USER_SCAN_META = None

    # clean modif from shell.cli.repl
    logtools.userlogger.reset()
    logtools.userlogger.disable()
    logtools.elogbook.disable()

    tango_attr_as_counter._TangoCounterControllerDict = weakref.WeakValueDictionary()
    BlissRepl.instance = None


@pytest.fixture(autouse=True)
def clean_tango():
    # close file descriptors left open by Tango (see tango-controls/pytango/issues/324)
    try:
        ApiUtil.cleanup()
    except RuntimeError:
        # no Tango ?
        pass


@pytest.fixture(autouse=True)
def clean_blissdata():
    blissdata_client.set_default_redis_connection_manager_callback(None)


@pytest.fixture(scope="session")
def homepage_port(ports):
    yield ports.homepage_port


@pytest.fixture(scope="session")
def beacon_tmpdir(tmpdir_factory):
    tmpdir = str(tmpdir_factory.mktemp("beacon"))
    yield tmpdir


@pytest.fixture(scope="session")
def beacon_directory(beacon_tmpdir):
    beacon_dir = os.path.join(beacon_tmpdir, "test_configuration")
    shutil.copytree(BEACON_DB_PATH, beacon_dir)
    yield beacon_dir


@pytest.fixture(scope="session")
def log_directory(beacon_tmpdir):
    log_dir = os.path.join(beacon_tmpdir, "log")
    os.mkdir(log_dir)
    yield log_dir


@pytest.fixture(scope="session")
def images_directory(tmpdir_factory):
    images_dir = os.path.join(str(tmpdir_factory.getbasetemp()), "images")
    shutil.copytree(IMAGES_PATH, images_dir)
    yield images_dir


@pytest.fixture(scope="session")
def ports(beacon_directory, log_directory):
    redis_uds = os.path.join(beacon_directory, ".redis.sock")
    redis_data_uds = os.path.join(beacon_directory, ".redis_data.sock")

    port_names = [
        "redis_port",
        "redis_data_port",
        "tango_port",
        "beacon_port",
        "logserver_port",
        "homepage_port",
    ]

    ports = namedtuple("Ports", " ".join(port_names))(*get_open_ports(6))
    args = [
        f"--port={ports.beacon_port}",
        f"--redis-port={ports.redis_port}",
        f"--redis-socket={redis_uds}",
        f"--redis-data-port={ports.redis_data_port}",
        f"--redis-data-socket={redis_data_uds}",
        f"--db-path={beacon_directory}",
        f"--tango-port={ports.tango_port}",
        f"--homepage-port={ports.homepage_port}",
        f"--log-server-port={ports.logserver_port}",
        f"--log-output-folder={log_directory}",
        "--log-level=WARN",
        "--tango-debug-level=0",
    ]
    proc = subprocess.Popen(BEACON + args)
    wait_ports(ports)

    # disable .rdb files saving (redis persistence)
    r = redis.Redis(host="localhost", port=ports.redis_port)
    r.config_set("SAVE", "")
    del r

    os.environ["TANGO_HOST"] = "localhost:%d" % ports.tango_port
    os.environ["BEACON_HOST"] = "localhost:%d" % ports.beacon_port

    yield ports

    atexit._run_exitfuncs()
    wait_terminate(proc)


def wait_ports(ports, timeout=10):
    with gevent.Timeout(timeout):
        wait_tcp_online("localhost", ports.beacon_port)
        wait_tango_db(port=ports.tango_port, db=2)


@pytest.fixture
def blissdata():
    blissdata_client.set_default_redis_connection_manager_callback(
        get_default_redis_connection_manager
    )


@pytest.fixture
def beacon(ports, beacon_directory, blissdata):
    redis_db = redis.Redis(port=ports.redis_port)
    redis_db.flushall()
    redis_data_db = redis.Redis(port=ports.redis_data_port)
    redis_data_db.flushall()
    static.Config.instance = None
    client._default_connection = connection.Connection("localhost", ports.beacon_port)
    config = static.get_config()
    yield config
    config.close()
    client._default_connection.close()
    # Ensure no connections are created due to garbage collection:
    client._default_connection = None

    # Always restore beacon directory
    rm_tree(Path(beacon_directory))  # still keeps Unix socket "files"
    # Restore files from BEACON_DB_PATH
    copy_tree(Path(BEACON_DB_PATH), Path(beacon_directory))


@pytest.fixture
def beacon_host_port(ports):
    return "localhost", ports.beacon_port


@pytest.fixture
def redis_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy()
    yield redis_conn


@pytest.fixture
def redis_data_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy(db=1)
    yield redis_conn


@pytest.fixture
def scan_tmpdir(tmpdir):
    yield tmpdir
    tmpdir.remove()


@contextmanager
def lima_simulator_context(personal_name, device_name):
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/LimaCCDs/{personal_name}"

    conda_env = os.environ.get("LIMA_SIMULATOR_CONDA_ENV")
    if not conda_env:
        conda_env = None
    conda = os.environ.get("CONDA_EXE", None)
    if conda_env and conda:
        if os.sep in conda_env:
            option = "-p"
        else:
            option = "-n"
        runner = [conda, "run", option, conda_env, "--no-capture-output", "LimaCCDs"]
    else:
        runner = ["LimaCCDs"]

    with start_tango_server(
        *runner,
        personal_name,
        # "-v4",               # to enable debug
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=None,
        check_children=conda_env is not None,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def lima_simulator2(ports):
    with lima_simulator_context("simulator2", "id00/limaccds/simulator2") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def bliss_tango_server(ports, beacon):
    device_name = "id00/bliss/test"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/bliss/test"

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.tango.servers.bliss_ds",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.STANDBY,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_tango_server(ports, beacon):

    device_name = "id00/tango/dummy"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_tg_server",
        "dummy",
        device_fqdn=device_fqdn,
        state=DevState.CLOSE,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_tango_server2(ports, beacon):

    device_name = "id00/tango/dummy2"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_tg_server",
        "dummy2",
        device_fqdn=device_fqdn,
        state=DevState.CLOSE,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def wago_tango_server(ports, default_session, wago_emulator):
    device_name = "1/1/wagodummy"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    # patching the property Iphost of wago tango device to connect to the mockup
    wago_ds = DeviceProxy(device_fqdn)
    wago_ds.put_property({"Iphost": f"{wago_emulator.host}:{wago_emulator.port}"})

    with start_tango_server(
        "Wago", "wago_tg_server", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def machinfo_tango_server(ports, beacon):
    device_name = "id00/tango/machinfo"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.machinfo_tg_server",
        "machinfo",
        device_fqdn=device_fqdn,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def regulation_tango_server(ports, beacon):
    device_name = "id00/regulation/loop"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/regulation/test"

    with start_tango_server(
        "Regulation",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def motor_tango_server(ports, beacon):
    device_name = "id00/tango/remo_ctrl"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/blissmotorcontroller/test"

    with start_tango_server(
        "BlissMotorController",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def session(beacon, scan_tmpdir):
    session = beacon.get("test_session")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    yield session
    session.close()


@pytest.fixture
def default_session(beacon, scan_tmpdir):
    default_session = session_module.DefaultSession()
    default_session.setup()
    default_session.scan_saving.base_path = str(scan_tmpdir)
    yield default_session
    default_session.close()


def pytest_addoption(parser):
    """
    Add pytest options
    """
    parser.addoption("--pepu", help="pepu host name")
    parser.addoption("--ct2", help="ct2 address")
    parser.addoption("--axis-name", help="axis name")
    parser.addoption("--axis-name2", help="axis name2")
    parser.addoption("--axis-name3", help="axis name3")
    parser.addoption("--mythen", action="store", help="mythen host name")
    parser.addoption(
        "--wago",
        help="connection information: tango_cpp_host:port,domani,wago_dns\nExample: --wago bibhelm:20000,ID31,wcid31c",
    )


@pytest.fixture
def alias_session(beacon, lima_simulator, scan_tmpdir):
    session = beacon.get("test_alias")
    env_dict = dict()
    session.setup(env_dict)
    session.scan_saving.base_path = str(scan_tmpdir)

    ls = env_dict["lima_simulator"]
    rois = ls.roi_counters
    r1 = Roi(0, 0, 100, 200)
    rois["r1"] = r1
    r2 = Roi(100, 100, 100, 200)
    rois["r2"] = r2
    r3 = Roi(200, 200, 200, 200)
    rois["r3"] = r3

    env_dict["ALIASES"].add("myroi", ls.counters.r1_sum)
    env_dict["ALIASES"].add("myroi3", ls.counters.r3_sum)

    yield session

    session.close()


@pytest.fixture
def wago_emulator(beacon):
    config_tree = beacon.get_config("wago_simulator")
    modules_config = ModulesConfig.from_config_tree(config_tree)
    wago = WagoEmulator(modules_config)

    yield wago

    wago.close()


@contextmanager
def flint_context(with_flint=True, stucked=False):
    """Helper to capture and clean up all new Flint process created during the
    context.

    It also provides few arguments to request a specific Flint state.
    """
    flint_singleton = proxy._get_singleton()

    pids = set()

    def register_new_flint_pid(pid):
        nonlocal pids
        pids.add(pid)

    assert flint_singleton._on_new_pid is None
    flint_singleton._on_new_pid = register_new_flint_pid

    if with_flint:
        flint = plot.get_flint()

    if stucked:
        assert with_flint is True
        try:
            with gevent.Timeout(seconds=0.1):
                # This command does not return that is why it is
                # aborted with a timeout
                flint.test_infinit_loop()
        except gevent.Timeout:
            pass
    yield
    for pid in pids:
        try:
            wait_terminate(pid, timeout=0.1)
        except gevent.Timeout:
            # This could happen, if the kill fails, after 10s
            pass

    flint_singleton._on_new_pid = None
    flint_singleton._proxy_cleanup()


@pytest.fixture
def flint_session(xvfb, beacon, scan_tmpdir):
    session = beacon.get("flint")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    with flint_context():
        yield session
    session.close()


@pytest.fixture
def test_session_with_flint(xvfb, session):
    with flint_context():
        yield session


@pytest.fixture
def test_session_with_stucked_flint(xvfb, session):
    with flint_context(stucked=True):
        yield session


@pytest.fixture
def test_session_without_flint(xvfb, session):
    """This session have to start without flint, but can finish with"""
    with flint_context(False):
        yield session


@pytest.fixture
def log_context():
    """
    Initialize BLISS logging and restore previous logging state on exit
    """
    # Save the logging context
    old_handlers = list(logging.getLogger().handlers)
    old_logger_dict = dict(logging.getLogger().manager.loggerDict)

    # Bliss __init__ module loads the global_log which creates
    # "global" and "global.controllers" BlissLoggers
    # so they exist in old_logger_dict but with only a NullHandler handler

    logging_startup()
    # this modifies the 'global' logger:
    #   - adding a beacon handler
    #   - changing level to logging.INFO
    #   - turning propagation to False

    # revert logging_startup modifications of the 'global' logger for the test purposes
    logging.getLogger("global").propagate = True
    logging.getLogger("global").setLevel(logging.NOTSET)
    logging.getLogger("global").handlers.remove(global_log._beacon_handler)

    yield

    global_log.restore_initial_state()
    # this removes the beacon handler from the 'global' logger

    # logging.getLogger("bliss").setLevel(logging.NOTSET)
    # logging.getLogger("flint").setLevel(logging.NOTSET)

    # logging.getLogger("parso.python.diff").disabled = False
    # logging.getLogger("parso.cache").disabled = False

    # Restore the logging context
    logging.shutdown()
    logging.setLoggerClass(logging.Logger)
    logging.getLogger().handlers.clear()  # deletes all handlers
    logging.getLogger().handlers.extend(old_handlers)
    logging.getLogger().manager.loggerDict.clear()  # deletes all loggers
    logging.getLogger().manager.loggerDict.update(old_logger_dict)


@pytest.fixture
def userlogger_enabled():
    """Enables user logging for any Bliss session"""
    logtools.userlogger.enable()
    try:
        yield
    finally:
        logtools.userlogger.disable()


@pytest.fixture
def elogbook_enabled():
    """Enables the Elogbook for any Bliss session"""
    logtools.elogbook.enable()
    try:
        yield
    finally:
        logtools.elogbook.disable()


@pytest.fixture
def log_shell_mode(userlogger_enabled, elogbook_enabled):
    pass


@pytest.fixture
def icat_mock_client(mocker, elogbook_enabled) -> MagicMock:
    """Enables the ICAT client and Elogbook for any Bliss session.
    The retured object can be used to check that all desired calls are being made."""
    config = static.get_config()
    config.root["icat_servers"] = {"disable": False}
    mockedclass = mocker.patch("bliss.icat.client.IcatClient")

    datasetids = None

    def store_dataset(path=None, **_):
        nonlocal datasetids
        if datasetids is None:
            datasetids = list()
        datasetids.append(DatasetId(name=os.path.basename(path), path=path))

    def registered_dataset_ids(**_):
        return datasetids

    mockedclass.return_value.store_dataset.side_effect = store_dataset

    mockedclass.return_value.registered_dataset_ids.side_effect = registered_dataset_ids

    return mockedclass


@pytest.fixture
def nexus_writer_service(ports):
    device_name = "id00/bliss_nxwriter/test_session"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        "NexusWriterService", "testwriters", "--log", "warning", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def simulation_monochromator(beacon):
    bragg = beacon.get("sim_mono")
    bragg.move(10)
    mono = beacon.get("simul_mono")
    mono.xtal.change("Si220")
    yield mono
    mono._close()
