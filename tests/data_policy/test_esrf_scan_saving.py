# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import time
import datetime
import pytest
import gevent
import gevent.queue

from bliss.scanning.scan_saving import ESRFDataPolicyEvent, ScanSaving
from bliss.config import channels
from bliss.shell.standard import (
    newproposal,
    newsample,
    newcollection,
    newdataset,
    enddataset,
    endproposal,
)

from . import icat_test_utils


def test_icat_typical_sequence(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    newproposal("totoproposal")
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    newdataset()
    icat_test_utils.create_dataset(scan_saving)
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    newdataset()
    icat_test_utils.create_dataset(scan_saving)
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    enddataset()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)


def test_inhouse_scan_saving(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    scan_saving_config = scan_saving.scan_saving_config
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    inhouse_name1 = f"{scan_saving.beamline}{time.strftime('%y%m')}"
    inhouse_name2 = scan_saving.beamline + "9999"
    for bset in [False, True]:
        if bset:
            scan_saving.proposal_name = inhouse_name2
            info = icat_test_utils.proposal_info(scan_saving)
            icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
            icat_mock_client.reset_mock()
        assert scan_saving.beamline == scan_saving_config["beamline"]
        assert scan_saving.proposal_type == "inhouse"
        if bset:
            assert scan_saving.proposal_name == inhouse_name2
        else:
            assert scan_saving.proposal_name == inhouse_name1
        assert scan_saving.base_path == scan_saving_config["inhouse_data_root"].format(
            beamline=scan_saving.beamline
        )
        assert scan_saving.icat_base_path == scan_saving_config[
            "inhouse_data_root"
        ].format(beamline=scan_saving.beamline)
        assert_default_sample_dataset(scan_saving)


def test_visitor_scan_saving(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "mx415"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert os.path.join(os.sep, "mx415", "") in scan_saving.proposal.path
    assert_default_sample_dataset(scan_saving)

    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()


def test_crg_visitor_scan_saving(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "00-1234"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert (
        os.path.join(os.sep, scan_saving.beamline + "1234", "")
        in scan_saving.proposal.path
    )
    assert_default_sample_dataset(scan_saving)

    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    scan_saving.proposal_name = "00-01234"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert (
        os.path.join(os.sep, scan_saving.beamline + "1234", "")
        in scan_saving.proposal.path
    )
    assert_default_sample_dataset(scan_saving)

    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "01-1234"  # not a valid CRG proposal at this beamline
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert os.path.join(os.sep, "011234", "") in scan_saving.proposal.path
    assert_default_sample_dataset(scan_saving)

    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()


def test_tmp_scan_saving(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "test123"
    assert scan_saving.proposal_type == "tmp"
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    expected = scan_saving_config["icat_tmp_data_root"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.icat_base_path == expected
    assert_default_sample_dataset(scan_saving)

    assert icat_mock_client.call_count == 0


def test_proposal_type(session, esrf_data_policy):
    scan_saving_config = session.scan_saving.scan_saving_config

    newproposal("blc123")
    assert session.scan_saving.proposal_type == "visitor"
    scan_saving_config["inhouse_proposal_prefixes"] = ["blc"]
    assert session.scan_saving.proposal_type == "inhouse"

    newproposal("ih123")
    assert session.scan_saving.proposal_type == "visitor"
    scan_saving_config["inhouse_proposal_prefixes"] = ["blc", "ih"]
    assert session.scan_saving.proposal_type == "inhouse"

    newproposal("ttt123")
    assert session.scan_saving.proposal_type == "visitor"
    scan_saving_config["tmp_proposal_prefixes"] = ["ttt"]
    assert session.scan_saving.proposal_type == "tmp"


def assert_default_sample_dataset(scan_saving):
    assert scan_saving.collection_name == "sample"


def test_auto_dataset_increment(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    assert scan_saving.dataset_name == "0001"
    with pytest.raises(AttributeError):
        scan_saving.template = "toto"
    assert scan_saving.get_path() == os.path.join(
        scan_saving.base_path,
        scan_saving.proposal_name,
        scan_saving.beamline,
        scan_saving.proposal_session_name,
        "raw",
        scan_saving.collection_name,
        f"{scan_saving.collection_name}_{scan_saving.dataset_name}",
    )
    scan_saving.collection_name = ""
    assert scan_saving.collection_name == "sample"
    scan_saving.dataset_name = ""
    assert scan_saving.dataset_name == "0001"
    with pytest.raises(AttributeError):
        scan_saving.template = ""
    assert scan_saving.get_path().endswith("0001")

    assert icat_mock_client.call_count == 0


def test_auto_dataset_increment_with_creation(
    session, esrf_data_policy, icat_mock_client
):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.dataset_name = ""
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    icat_test_utils.create_dataset(scan_saving)
    path = scan_saving.get_path()
    new_filename = os.path.join(path, scan_saving.data_filename + ".h5")
    info = icat_test_utils.dataset_info(scan_saving)
    with open(new_filename, "w"):
        scan_saving.dataset_name = ""
        assert scan_saving.dataset_name == "0003"
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.dataset_name = "dataset"
    icat_test_utils.create_dataset(scan_saving)
    info = icat_test_utils.dataset_info(scan_saving)
    assert icat_mock_client.return_value.store_dataset.call_count == 0

    scan_saving.dataset_name = "dataset"
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.dataset_name == "dataset_0002"
    assert scan_saving.get_path().endswith("dataset_0002")


@pytest.mark.parametrize(
    "user_action,same_sample",
    [[newdataset, True], [newsample, True], [newproposal, False], [newproposal, True]],
)
def test_close_dataset(
    session, esrf_data_policy, icat_mock_client, user_action, same_sample
):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    newproposal("myproposal")
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    if same_sample:
        newsample("mysample")
        info = icat_test_utils.collection_info(scan_saving)
        icat_test_utils.assert_collection_elog(icat_mock_client, **info)
        icat_mock_client.reset_mock()

    icat_test_utils.create_dataset(scan_saving)
    info = icat_test_utils.dataset_info(scan_saving)
    user_action(None)
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    if user_action is newdataset:
        info = icat_test_utils.dataset_info(scan_saving)
        icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    elif user_action is newsample:
        info = icat_test_utils.collection_info(scan_saving)
        icat_test_utils.assert_collection_elog(icat_mock_client, **info)
    else:
        info = icat_test_utils.proposal_info(scan_saving)
        icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
        icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()


def test_data_policy_user_functions(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)
    default_proposal = f"{scan_saving.beamline}{time.strftime('%y%m')}"

    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    newproposal("toto")
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    newcollection("tata")
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.collection_info(scan_saving)
    icat_test_utils.assert_collection_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "tata"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    newdataset("tutu")
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    info = icat_test_utils.dataset_info(scan_saving)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "tata"
    assert scan_saving.dataset_name == "tutu"
    icat_test_utils.create_dataset(scan_saving)

    newproposal()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    enddataset()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    info = icat_test_utils.dataset_info(scan_saving)
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0003"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    endproposal()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    info = icat_test_utils.dataset_info(scan_saving)
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0004"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    newproposal("toto")
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    endproposal()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0005"


def test_data_policy_repeat_user_functions(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)
    default_proposal = f"{scan_saving.beamline}{time.strftime('%y%m')}"

    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description is None

    scan_saving.newproposal(None)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_mock_client.return_value.start_investigation.call_count == 0
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    scan_saving.newcollection(None)
    info = icat_test_utils.collection_info(scan_saving)
    icat_test_utils.assert_collection_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    scan_saving.newdataset(None)
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description is None

    scan_saving.newsample(None, description="sample description")
    info = icat_test_utils.collection_info(scan_saving)
    icat_test_utils.assert_collection_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "sample description"

    scan_saving.newsample(None, description="modified sample description")
    info = icat_test_utils.collection_info(scan_saving)
    icat_test_utils.assert_collection_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description == "modified sample description"
    assert scan_saving.dataset.description == "modified sample description"

    scan_saving.newdataset(None, description="toto")
    assert icat_mock_client.return_value.store_dataset.call_count == 0
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description == "modified sample description"
    assert scan_saving.dataset.description == "modified sample description (toto)"


def test_fresh_sample(session, esrf_data_policy):
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.newsample("toto")
    assert scan_saving.collection_name == "toto"
    assert scan_saving.dataset_name == "0001"


def test_fresh_newcollection(session, esrf_data_policy):
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.newcollection("toto")
    assert scan_saving.collection_name == "toto"
    assert scan_saving.dataset_name == "0001"


def test_fresh_newdataset(session, esrf_data_policy):
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.newdataset("toto")
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "toto"


def test_data_policy_name_validation(session, esrf_data_policy):
    # `set_expiration_time` is called in this test to test that the names
    # don't cause Redis key search errors (issue #3358)

    scan_saving = session.scan_saving

    # proposal name validation (alphanumeric, space-like, dash and underscore)
    invalid_names = "with,", "with:", "with;", f"with{os.sep}", "withé"
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.proposal_name = name

    # proposal name normalization
    valid_names = "HG64", " HG- 64", "HG__64", "hg_64", "  H -- G   -- 6_4  "
    for name in valid_names:
        scan_saving.proposal_name = name
        assert scan_saving.proposal_name == "hg64"
        scan_saving.proposal.set_expiration_time()

    # beamline name validation (alphanumeric, space-like, dash and underscore)
    invalid_names = "with,", "with:", "with;", f"with{os.sep}", "withé"
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.proposal_name = name

    # beamline name normalization
    valid_names = "ID00", " ID- 00", "ID__00", "ID_00", "  I -- D   -- 0_0  "
    for name in valid_names:
        scan_saving.proposal_name = name
        assert scan_saving.proposal_name == "id00"
        scan_saving.proposal.set_expiration_time()

    # collection name validation (no os.sep)
    invalid_names = (f"with{os.sep}", "with}", "with{")
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.collection_name = name

    # collection name normalization (not normalized!)
    valid_names = (
        "with,",
        "with:",
        "with;",
        "withé",
        " sample Name",
        "sample  Name",
        "  sample -- Name ",
    )
    for name in valid_names:
        scan_saving.collection_name = name
        assert scan_saving.collection_name == name
        scan_saving.collection.set_expiration_time()

    # dataset name validation (no os.sep)
    invalid_names = (f"with{os.sep}", "with}", "with{")
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.collection_name = name

    # dataset name normalization (not normalized!)
    valid_names = (
        "with,",
        "with:",
        "with;",
        "withé",
        "with)",
        " dataset Name",
        "dataset  Name",
        "  dataset -- Name ",
    )
    for name in valid_names:
        scan_saving.dataset_name = name
        assert scan_saving.dataset_name == name
        scan_saving.dataset.set_expiration_time()


def test_session_scan_saving_clone(session, esrf_data_policy):
    scan_saving = session.scan_saving

    # just to create a tango dev proxy in scan saving
    scan_saving.icat_client

    # create a clone
    scan_saving2 = scan_saving.clone()

    # check that the clone is a clone
    # and that the SLOTS are the same (shallow copy)
    assert id(scan_saving) != id(scan_saving2)
    assert scan_saving2._icat_client is not None
    assert id(scan_saving._icat_client) == id(scan_saving2._icat_client)

    # check that the same redis structure is used by the clone
    scan_saving.proposal_name = "toto"
    assert scan_saving2.proposal_name == "toto"


def test_mount_points(session, esrf_data_policy):
    scan_saving = session.scan_saving
    scan_saving_config = scan_saving.scan_saving_config

    # Test setting mount points
    assert scan_saving.mount_points == {"", "fs1", "fs2", "fs3"}
    for mp in ["", "fs1", "fs2", "fs3"]:
        scan_saving.mount_point = mp
        assert scan_saving.mount_point == mp
    with pytest.raises(ValueError):
        scan_saving.mount_point = "non-existing"
    scan_saving.mount_point == mp

    # Test temp mount points (has sf1 and sf2 and fixed icat)
    scan_saving.proposal_name = "temp123"

    icat_expected = scan_saving_config["icat_tmp_data_root"].format(
        beamline=scan_saving.beamline
    )

    scan_saving.mount_point = ""
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    scan_saving.mount_point = "fs1"
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    scan_saving.mount_point = "fs2"
    expected = scan_saving_config["tmp_data_root"]["fs2"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    scan_saving.mount_point = "fs3"
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    # Test visitor mount points (has sf1 and sf3 and no fixed icat)
    scan_saving.proposal_name = "hg123"

    scan_saving.mount_point = ""
    expected = scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs1"
    expected = scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs2"
    expected = scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs3"
    expected = scan_saving_config["visitor_data_root"]["fs3"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    # Test inhouse mount points (no named mount points)
    scan_saving.proposal_name = scan_saving.beamline + "9999"

    expected = scan_saving_config["inhouse_data_root"].format(
        beamline=scan_saving.beamline
    )

    scan_saving.mount_point = ""
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs1"
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs2"
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs3"
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected


def test_session_ending(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)
    default_proposal = f"{scan_saving.beamline}{time.strftime('%y%m')}"

    scan_saving.newproposal("hg123")
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    scan_saving.newcollection("sample1")
    info = icat_test_utils.collection_info(scan_saving)
    icat_test_utils.assert_collection_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == "hg123"
    assert scan_saving.collection_name == "sample1"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.enddataset()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == "hg123"
    assert scan_saving.collection_name == "sample1"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.create_dataset(scan_saving)

    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.endproposal()
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"


def test_data_policy_event(session, esrf_data_policy):
    event_channel = channels.EventChannel(f"{session.name}:esrf_data_policy")
    event_queue = gevent.queue.Queue()

    def event_callback(events):
        for event in events:
            event_queue.put(event)

    event_channel.register_callback(event_callback)

    scan_saving = session.scan_saving
    scan_saving.newproposal("hg123")
    scan_saving.newcollection("sample1")
    scan_saving.newdataset("42")
    icat_test_utils.create_dataset(scan_saving)
    scan_saving.enddataset()
    icat_test_utils.create_dataset(scan_saving)
    scan_saving.endproposal()

    session.disable_esrf_data_policy()

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Enable

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    msg = f"Proposal set to 'hg123' (session '{scan_saving.proposal_session_name}')"
    assert event["value"]["message"] == msg

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    assert event["value"]["message"] == "Dataset collection set to 'sample1'"

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    assert event["value"]["message"] == "Dataset set to '0042'"

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    assert event["value"]["message"] == "Dataset set to '0001'"

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    msg = f"Proposal set to '{scan_saving.proposal_name}' (session '{scan_saving.proposal_session_name}')"
    assert event["value"]["message"] == msg

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Disable


def test_date_in_basepath(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Put date in base path template:
    new_base_path = os.path.join(scan_saving.base_path, "{date}")
    scan_saving.scan_saving_config["inhouse_data_root"] = new_base_path

    # Call newproposal in the past:
    pasttime = time.time() - 3600 * 24 * 100

    def mytime():
        return pasttime

    time.time, orgtime = mytime, time.time
    inhouse_name1 = scan_saving.beamline + "9999"
    try:
        scan_saving.newproposal(inhouse_name1)
        info = icat_test_utils.proposal_info(scan_saving)
        icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
        icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
        icat_mock_client.reset_mock()
        past = scan_saving.date
        assert scan_saving.base_path.endswith(past)
    finally:
        time.time = orgtime

    # Call newproposal in the present:
    scan_saving.newproposal(inhouse_name1)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_mock_client.return_value.start_investigation.call_count == 0
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.date == past
    assert scan_saving.base_path.endswith(past)

    inhouse_name2 = scan_saving.beamline + "8888"
    scan_saving.newproposal(inhouse_name2)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.date != past
    assert not scan_saving.base_path.endswith(past)

    scan_saving.newproposal(inhouse_name1)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.date != past
    assert not scan_saving.base_path.endswith(past)


def test_parallel_sessions(
    session, session2, esrf_data_policy, esrf_data_policy2, icat_mock_client
):
    def get_scan_saving1():
        return session.scan_saving

    def get_scan_saving2():
        return session2.scan_saving

    scan_saving = get_scan_saving1()
    assert scan_saving.session == "test_session"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    scan_saving = get_scan_saving2()
    assert scan_saving.session == "test_session2"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    scan_saving = get_scan_saving1()
    inhouse_name = scan_saving.beamline + "9999"
    scan_saving.newproposal(inhouse_name)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.dataset_name == "0001"

    scan_saving = get_scan_saving2()
    scan_saving.newproposal(inhouse_name)
    info = icat_test_utils.proposal_info(scan_saving)
    icat_test_utils.assert_proposal_icat(icat_mock_client, **info)
    icat_test_utils.assert_proposal_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert scan_saving.dataset_name == "0002"

    scan_saving = get_scan_saving1()
    scan_saving.newdataset(None)
    icat_mock_client.return_value.send_message.call_count == 0
    scan_saving = get_scan_saving2()
    scan_saving.newdataset(None)
    icat_mock_client.return_value.send_message.call_count == 0
    assert get_scan_saving1().dataset_name == "0001"
    assert get_scan_saving2().dataset_name == "0002"

    scan_saving = get_scan_saving2()
    scan_saving.newdataset(None)
    icat_mock_client.return_value.send_message.call_count == 0
    scan_saving = get_scan_saving1()
    scan_saving.newdataset(None)
    icat_mock_client.return_value.send_message.call_count == 0
    assert get_scan_saving1().dataset_name == "0001"
    assert get_scan_saving2().dataset_name == "0002"

    scan_saving = get_scan_saving1()
    icat_test_utils.create_dataset(scan_saving)
    icat_mock_client.reset_mock()  # TODO: too many elog messages
    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.newdataset(None)
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert get_scan_saving1().dataset_name == "0003"
    assert get_scan_saving2().dataset_name == "0002"

    scan_saving = get_scan_saving1()
    scan_saving.newdataset("0002")
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert get_scan_saving1().dataset_name == "0003"
    assert get_scan_saving2().dataset_name == "0002"

    scan_saving = get_scan_saving1()
    scan_saving.newdataset("named")
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert get_scan_saving1().dataset_name == "named"
    assert get_scan_saving2().dataset_name == "0002"

    scan_saving = get_scan_saving2()
    scan_saving.newdataset("named")
    info = icat_test_utils.dataset_info(scan_saving)
    icat_test_utils.assert_dataset_elog(icat_mock_client, **info)
    icat_mock_client.reset_mock()
    assert get_scan_saving1().dataset_name == "named"
    assert get_scan_saving2().dataset_name == "named_0002"


def test_parallel_scans(session, esrf_data_policy):
    glts = [
        gevent.spawn(session.scan_saving.clone().on_scan_run, True) for _ in range(100)
    ]
    gevent.joinall(glts, raise_error=True, timeout=10)


def test_proposal_session(session, esrf_data_policy):
    scan_saving = session.scan_saving
    proposal_name1 = "hg999"
    proposal_name2 = "hg888"
    proposal_name3 = "hg777"
    dt = datetime.datetime.now().replace(day=1)
    year = dt.year
    session_name1 = dt.strftime(scan_saving._proposal_session_name_format)
    dt = dt.replace(year=dt.year - 1)
    session_name2 = dt.strftime(scan_saving._proposal_session_name_format)
    dt = dt.replace(year=dt.year - 1)
    session_name3a = dt.strftime(scan_saving._proposal_session_name_format)
    dt = dt.replace(year=year + 1)
    session_name3b = dt.strftime(scan_saving._proposal_session_name_format)

    # Session directory does not exist
    scan_saving.proposal_name = proposal_name1
    root_path1 = scan_saving.root_path
    assert scan_saving.proposal_session_name == session_name1

    root_path2a = root_path1.replace(proposal_name1, proposal_name2)
    root_path2b = root_path2a.replace(session_name1, session_name2)
    os.makedirs(root_path2a)

    root_path3a = root_path1.replace(proposal_name1, proposal_name3)
    root_path3a = root_path3a.replace(session_name1, session_name3a)
    root_path3b = root_path3a.replace(session_name3a, session_name3b)
    os.makedirs(root_path3a)
    os.makedirs(root_path3b)

    # Session directory exists
    scan_saving.proposal_name = proposal_name2
    assert scan_saving.proposal_name == proposal_name2
    assert scan_saving.proposal_session_name == session_name1
    actual = os.path.normpath(os.path.join(scan_saving.root_path, ".."))
    expected = os.path.normpath(os.path.join(root_path2a, ".."))
    assert actual == expected

    # Session directory does not exist
    scan_saving.proposal_session_name = session_name2
    assert scan_saving.proposal_session_name == session_name1
    os.makedirs(root_path2b)

    # Session directory exists
    scan_saving.proposal_session_name = session_name2
    assert scan_saving.proposal_session_name == session_name2
    actual = os.path.normpath(os.path.join(scan_saving.root_path, ".."))
    expected = os.path.normpath(os.path.join(root_path2b, ".."))
    assert actual == expected

    # Session directory does not exist
    scan_saving.proposal_session_name = session_name3a
    assert scan_saving.proposal_session_name == session_name1

    # Multiple session directories exist
    scan_saving.proposal_name = proposal_name3
    assert scan_saving.proposal_name == proposal_name3
    assert scan_saving.proposal_session_name == session_name3a
    actual = os.path.normpath(os.path.join(scan_saving.root_path, ".."))
    expected = os.path.normpath(os.path.join(root_path3a, ".."))
    assert actual == expected


def test_proposal_session_today(session, esrf_data_policy):
    scan_saving = session.scan_saving
    scan_saving.newproposal("hg999")
    root_path = scan_saving.root_path
    session_current = scan_saving.proposal_session_name

    today = datetime.datetime.now()
    oneday = datetime.timedelta(days=1, minutes=1)
    yesterday = today - oneday
    tomorrow = today + oneday
    session_today = today.strftime(scan_saving._proposal_session_name_format)
    session_yesterday = yesterday.strftime(scan_saving._proposal_session_name_format)
    session_tomorrow = tomorrow.strftime(scan_saving._proposal_session_name_format)

    root_path_today = root_path.replace(session_current, session_today)
    root_path_yesterday = root_path.replace(session_current, session_yesterday)
    root_path_tomorrow = root_path.replace(session_current, session_tomorrow)

    os.makedirs(root_path_today)
    scan_saving.newproposal("hg999")
    assert scan_saving.proposal_session_name == session_today

    os.makedirs(root_path_tomorrow)
    scan_saving.newproposal("hg999")
    assert scan_saving.proposal_session_name == session_today

    os.makedirs(root_path_yesterday)
    scan_saving.newproposal("hg999")
    assert scan_saving.proposal_session_name == session_today

    scan_saving.newproposal("hg999", session_name=session_yesterday)
    assert scan_saving.proposal_session_name == session_yesterday

    scan_saving.newproposal("hg999", session_name=session_tomorrow)
    assert scan_saving.proposal_session_name == session_tomorrow

    scan_saving.newproposal("hg999")
    assert scan_saving.proposal_session_name == session_today
