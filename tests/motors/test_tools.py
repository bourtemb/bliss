# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.common import scans
from bliss.scanning.scan_tools import last_scan_motors
from bliss.controllers.motor import get_real_axes


def test_last_scan_motors(session, calc_mot2, calc_mot1, roby):
    # calc_mot2 depends on calc_mot1, which depends on roby

    simul_counter = session.env_dict["diode"]
    scans.ascan(calc_mot2, 0, 1, 3, 0, simul_counter, save=False)
    assert list(m.name for m in last_scan_motors()) == [
        "calc_mot2",
        "calc_mot1",
        "roby",
    ]
    # assert list(m.name for m in last_scan_motors(depth=1)) == ["calc_mot2"]
    # assert list(m.name for m in last_scan_motors(depth=2)) == ["calc_mot2", "calc_mot1"]
    # assert list(m.name for m in last_scan_motors(depth=3)) == ["calc_mot2", "calc_mot1", "roby"]


def test_get_real_axes(session, s1f, s1ho, calc_mot2):
    assert set(m.name for m in get_real_axes(s1f, s1ho)) == set(
        ["s1b", "s1f", "s1u", "s1d"]
    )
    # the real axis of calc_mot2 is a calc
    assert list(m.name for m in get_real_axes(calc_mot2, depth=1)) == ["calc_mot1"]
    assert list(m.name for m in get_real_axes(calc_mot2)) == ["calc_mot1", "roby"]
    assert list(m.name for m in get_real_axes(calc_mot2, depth=2)) == [
        "calc_mot1",
        "roby",
    ]


def test_get_data_axes(session, calc_mot2, calc_mot1, roby):
    # calc_mot2 depends on calc_mot1, which depends on roby

    simul_counter = session.env_dict["diode"]

    s = scans.ascan(calc_mot2, 0, 1, 3, 0, simul_counter, run=False, save=False)

    assert s._get_data_axes() == [calc_mot2]
    assert s._get_data_axes(include_calc_reals=True) == [calc_mot2, calc_mot1, roby]
    assert s._get_data_axes(include_calc_reals=1) == [calc_mot2, calc_mot1]
    assert s._get_data_axes(include_calc_reals=2) == [calc_mot2, calc_mot1, roby]
