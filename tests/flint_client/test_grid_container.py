"""Testing custom plots provided by Flint."""

from bliss.common import plot


def test_empty_grid(flint_session):
    """Create and close a grid.

    Check that the amount of plot is right
    """
    flint = plot.get_flint()
    p = flint.get_plot("grid")
    assert flint.test_count_custom_plots() == 1
    assert p.is_open()
    p.close()
    assert flint.test_count_custom_plots() == 0
    assert not p.is_open()


def test_plots_in_grid(flint_session):
    """Create a grid with 4 plots.

    Close one of then, close the parent.

    Check that the amount of plot is right
    """
    flint = plot.get_flint()
    p = flint.get_plot("grid")
    p1 = p.get_plot("curve", row=0, col=0)
    p2 = p.get_plot("curve", row=0, col=1)
    p3 = p.get_plot("curve", row=0, col=0, col_span=2)
    assert p1
    assert p2
    assert p3
    assert flint.test_count_custom_plots() == 4
    assert p3.is_open()
    p3.close()
    assert flint.test_count_custom_plots() == 3
    assert not p3.is_open()
    p.close()
    assert flint.test_count_custom_plots() == 0
    assert not p2.is_open()


def test_curve_in_grid(flint_session):
    """Create a curve plot in a grid.

    Check that api is callable.
    """
    flint = plot.get_flint()
    p = flint.get_plot("grid")
    p1 = p.get_plot("curve", row=0, col=0)
    p1.add_curve([1, 2, 3, 4], [0, 3, 0, 6])


def test_image_in_grid(flint_session):
    """Create an image plot in a grid.

    Check that api is callable.
    """
    flint = plot.get_flint()
    p = flint.get_plot("grid")
    p1 = p.get_plot("image", row=0, col=0)
    p1.set_data([[1, 2, 1], [0, 2, 0], [3, 0, 3]])
