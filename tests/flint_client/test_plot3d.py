"""Testing scatter3D plot provided by Flint."""

import numpy
from bliss.common import plot


def test_empty(flint_session):
    """Create and empty plot3d"""
    flint = plot.get_flint()
    p = flint.get_plot("plot3d")
    numpy.testing.assert_almost_equal(p.get_data_range(), [[0, 0, 0], [1, 1, 1]])


def test_scatter(flint_session):
    """Create a plot3d and use some API

    Check that the data range is right
    """
    flint = plot.get_flint()
    p = flint.get_plot("plot3d")
    p.add_scatter_item("a", "b", "c", "d", symbol="o", lut="cividis", vmin=0, vmax=10)
    p.set_data(a=[0, 1], b=[2, 3], c=[4, 5], d=[6, 7])
    numpy.testing.assert_almost_equal(p.get_data_range(), [[0, 2, 4], [1, 3, 5]])


def test_clear_scatter(flint_session):
    """Create a plot3d with data and clear it.

    Check that the plot is empty
    """
    flint = plot.get_flint()
    p = flint.get_plot("plot3d")
    p.add_scatter_item("a", "b", "c", "d", legend="s1")
    p.set_data(a=[0, 1], b=[2, 3], c=[4, 5], d=[6, 7])
    p.remove_item("s1")
    numpy.testing.assert_almost_equal(p.get_data_range(), [[0, 0, 0], [1, 1, 1]])
