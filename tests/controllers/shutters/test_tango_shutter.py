# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import logging
from bliss.common.shutter import BaseShutterState


def test_tango_shutter(beacon, dummy_tango_server, caplog):
    sh = beacon.get("safshut")

    assert sh.name == "safshut"
    assert sh.config["name"] == "safshut"

    sh.open(timeout=4)
    assert sh.state == BaseShutterState.OPEN
    assert sh.state_string[0] == BaseShutterState.OPEN.value
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.open()
        assert sh.state == BaseShutterState.OPEN
    assert "ignored" in caplog.messages[-1]

    sh.close(timeout=4)
    assert sh.state == BaseShutterState.CLOSED
    assert sh.state_string[0] == BaseShutterState.CLOSED.value
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.close()
        assert sh.state == BaseShutterState.CLOSED
    assert "ignored" in caplog.messages[-1]

    assert isinstance(sh.state_string, tuple)

    sh.proxy.setDisabled(True)
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.open()
        assert sh.state == BaseShutterState.DISABLE
    assert "ignored" in caplog.messages[-1]
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.close()
        assert sh.state == BaseShutterState.DISABLE
    assert "ignored" in caplog.messages[-1]
