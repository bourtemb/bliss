from bliss.shell.cli.repl import format_repl

from bliss.physics.units import ur


class WithInfo:
    def __info__(self):
        return "with info"


class WithRepr:
    def __repr__(self):
        return "with repr"


def test_format_repl_with_info():
    i = WithInfo()
    assert repr(format_repl(i)) == "with info"


def test_format_repl_with_repr():
    r = WithRepr()
    assert repr(format_repl(r)) == "with repr"


def test_format_repl_with_quantity():
    q = ur.Quantity(1.2345, ur.kg * ur.m / ur.s**2)
    assert repr(format_repl(q)) == f"{q:~P}"
