import contextlib
from prompt_toolkit.input.defaults import create_pipe_input
from prompt_toolkit.output import DummyOutput
from bliss.shell.cli.repl import BlissRepl


@contextlib.contextmanager
def bliss_repl(locals_dict=None):
    if locals_dict is None:
        locals_dict = {}
    with create_pipe_input() as inp:
        try:
            br = BlissRepl(
                input=inp,
                output=DummyOutput(),
                style="default",
                get_globals=lambda: locals_dict,
            )
            yield br
        finally:
            BlissRepl.instance = None  # BlissRepl is a Singleton
