# the next line tests issue #2888
DEFAULT_CHAIN  # noqa: F821

# test issue 1583
ERROR_REPORT  # noqa: F821

raise RuntimeError("Uncaught exception during setup.")
